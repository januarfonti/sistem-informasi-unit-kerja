<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Issue_model extends CI_Model {

	var $table = 'siuk_issue';
	//var $column = array('firstname','lastname','gender','address','dob'); //set column field database for order and search
	var $order = array('id' => 'desc'); // default order

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_dataapp()
	{
		return $this->db->query("SELECT
		SUM(IF(MONTH = 1, numRecords, 0)) AS 'January',
    SUM(IF(MONTH = 2, numRecords, 0)) AS 'Feburary',
    SUM(IF(MONTH = 3, numRecords, 0)) AS 'March',
    SUM(IF(MONTH = 4, numRecords, 0)) AS 'April',
    SUM(IF(MONTH = 5, numRecords, 0)) AS 'May',
    SUM(IF(MONTH = 6, numRecords, 0)) AS 'June',
    SUM(IF(MONTH = 7, numRecords, 0)) AS 'July',
    SUM(IF(MONTH = 8, numRecords, 0)) AS 'August',
    SUM(IF(MONTH = 9, numRecords, 0)) AS 'September',
    SUM(IF(MONTH = 10, numRecords, 0)) AS 'October',
    SUM(IF(MONTH = 11, numRecords, 0)) AS 'November',
    SUM(IF(MONTH = 12, numRecords, 0)) AS 'December'
    FROM (
        SELECT id, MONTH(date) AS MONTH, COUNT(*) AS numRecords
        FROM siuk_issue
         WHERE category_issue = 'Application'
         AND YEAR(date) = '2016'

                 GROUP BY id, MONTH
    ) tabel")->row();
	}

	public function get_issue()
	{
		$this->db->from($this->table);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function update_status($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}


}
