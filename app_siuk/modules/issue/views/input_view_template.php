<section class="content">
    <h1 class="page-header"><i class="fa fa-plus"></i> INPUT ISSUE</h1>
    <?php echo $this->session->flashdata('pesan'); ?>
    <?php echo form_open('issue/save'); ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>ISSUE</label>
                    <input type="text" class="form-control" name="issue" data-validation="required">
                </div>
                <div class="form-group">
                    <label>CATEGORY ISSUE</label>
                    <select class="form-control" name="category_issue">
                      <option></option>
                      <option value="Infrastructure">Infrastructure</option>
                      <option value="Application">Application</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>LOKASI</label>
                    <select class="form-control" name="lokasi">
                      <option></option>
                      <option value="Gresik">Gresik</option>
                      <option value="Tuban">Tuban</option>
                      <option value="Padang">Padang</option>
                      <option value="Tonasa">Tonasa</option>
                      <option value="Rembang">Rembang</option>
                      <option value="Aceh">Aceh</option>
                      <option value="TLCC">TLCC</option>
                      <option value="ALL">ALL</option>
                    </select>
                </div>



            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label>PIC</label>
                  <input type="text" class="form-control" name="pic" required>
              </div>
              <div class="form-group">
                  <label>DATE</label>
                  <input type="text" class="form-control datepicker" name="date" required>
              </div>
              <div class="bootstrap-timepicker">
              <div class="form-group">
                  <label>TIME</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" name="time">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
            </div>
        </div>
        <div class="form-group">
            <label>DESKRIPSI ISSUE</label>
            <textarea required name="deskripsi_issue" class="textarea" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
        </div>
        <div class="form-group">
            <label>STATUS</label>
            <select class="form-control" name="status">
              <option></option>
              <option value="FINISH">FINISH</option>
              <option value="PROGRESS">PROGRESS</option>
              <option value="ON HOLD">ON HOLD</option>
            </select>
        </div>


        <button type="submit" class="btn btn-flat btn-block btn-lg">Submit</button>
    <?php echo form_close(); ?>

</section>
