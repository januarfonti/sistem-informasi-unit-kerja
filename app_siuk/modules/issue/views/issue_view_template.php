    <section class="content">
        <h1 class="page-header"><i class="fa fa-check-square"></i> ISSUE LAYANAN</h1>
        <?php echo $this->session->flashdata('pesan'); ?>

        <div class="box box-info">
           <div class="box-body">
             <table id="tabel" class="table table-hover">
               <thead>
                 <tr>
                   <th>No</th>
                   <th>Issue</th>
                   <th>Category Issue</th>
                   <th>Lokasi</th>
                   <th>Date</th>
                   <th>Time</th>
                   <th>Status</th>
                   <th>#</th>
                 </tr>
               </thead>
               <tbody>
                 <?php if (isset($list)) { $no=1; foreach($list as $row) { ?>
                 <tr>
                   <td><?php echo $no++; ?></td>
                   <td><?php echo $row->issue; ?></td>
                   <td><?php echo $row->category_issue; ?></td>
                   <td><?php echo $row->lokasi; ?></td>
                   <td><?php echo $row->date; ?></td>
                   <td><?php echo $row->time; ?></td>
                   <td><?php echo $row->status; ?></td>
                   <td><a href="<?php echo base_url('issue/detail/'.$row->id); ?>" class="btn btn-info">DETAIL</a></td>
                 </tr>
                 <?php } } ?>

               </tbody>
               <tfoot>
                 <tr>
                   <th>No</th>
                   <th>Issue</th>
                   <th>Jenis Layanan</th>
                   <th>Lokasi</th>
                   <th>Waktu</th>
                   <th>PIC</th>
                   <th>Status</th>
                   <th>#</th>
                 </tr>
               </tfoot>
             </table>
           </div>
           <!-- /.box-body -->
         </div>
         <!-- /.box -->
    </section>
