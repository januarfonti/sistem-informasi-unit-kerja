<?php if (isset($list)) { ?>
<section class="content">
    <h1 class="page-header"><i class="fa fa-plus"></i> UPDATE ISSUE</h1>
    <?php echo $this->session->flashdata('pesan'); ?>
    <?php echo form_open('issue/update'); ?>
    <input name="id" value="<?php echo $list->id; ?>" type="hidden">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>ISSUE</label>
                    <input type="text" class="form-control" name="issue" data-validation="required" value="<?php echo $list->issue; ?>">
                </div>
                <div class="form-group">
                    <label>JENIS LAYANAN</label>
                    <input type="text" class="form-control" name="jenis_layanan" required  value="<?php echo $list->jenis_layanan; ?>">
                </div>
                <div class="form-group">
                    <label>LOKASI</label>
                    <input type="text" class="form-control" name="lokasi" required value="<?php echo $list->lokasi; ?>">
                </div>



            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label>PIC</label>
                  <input type="text" class="form-control" name="pic" required value="<?php echo $list->pic; ?>">
              </div>
              <div class="form-group">
                  <label>WAKTU TERJADI</label>
                  <input type="text" class="form-control datepicker" name="waktu_terjadi" required  value="<?php echo $list->waktu_terjadi; ?>">
              </div>
              <div class="form-group">
                  <label>TARGET SELESAI</label>
                  <input type="text" class="form-control datepicker" name="target_selesai" required value="<?php echo $list->target_selesai; ?>">
              </div>
            </div>
        </div>
        <div class="form-group">
            <label>ACTION PLAN</label>
            <input type="text" class="form-control" name="action_plan" required value="<?php echo $list->action_plan; ?>">
        </div>
        <div class="form-group">
            <label>DESKRIPSI ISSUE</label>
            <textarea required name="deskripsi_issue" class="textarea" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $list->deskripsi_issue; ?></textarea>
        </div>


        <button type="submit" class="btn btn-flat btn-block btn-lg">Submit</button>
    <?php echo form_close(); ?>

</section>
<?php } ?>
