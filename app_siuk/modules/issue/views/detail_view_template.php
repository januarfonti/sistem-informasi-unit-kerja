<?php if (isset($list)) { ?>
<section class="content">
    <h1 class="page-header"><i class="fa fa-check-square"></i> ISSUE LAYANAN</h1>

      <div class="box box-info">
         <div class="box-body">
           <div class="row">
             <div class="col-md-6">
               <strong><?php echo $list->issue; ?></strong>
             </div>
             <div class="col-md-3">
               <strong><?php echo "PIC : ".$list->pic; ?></strong>
             </div>
             <div class="col-md-3">
               <strong><?php echo $list->lokasi." ".$list->date." ".$list->time; ?></strong>
             </div>

           </div>
           <br><br>
           <p><strong>Deskripsi Issue : </strong></p>
           <?php echo $list->deskripsi_issue; ?>

           <p><strong>Status : </strong><?php echo $list->status; ?>

         </div>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->


</section>
  <?php } ?>
