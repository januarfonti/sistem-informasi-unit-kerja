<script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.27/vue.min.js"></script>
<script src="<?php echo base_url(); ?>assets_theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?php echo base_url(); ?>assets_theme/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets_theme/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url(); ?>assets_theme/plugins/select2/select2.full.min.js"></script>

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>

<script>
    $(document).ready(function(){
    $('#tabel').DataTable();
    });
</script>

<script>
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
        $(".select2").select2();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            toggleActive : true,
            autoClose : true
        });

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });

        new Vue({
            el:'#app'
        });
      });
</script>
