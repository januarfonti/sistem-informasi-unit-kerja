<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Issue extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Issue_model','issue');
	}

	public function index()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('issue_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('issue_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->issue->get_issue();
    $this->load_view('issue_view_template',$data);
	}


	public function input()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('issue_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('issue_view_js',array(),true);
    $this->add_template_html_footer($p_js);
    $this->load_view('input_view_template');
	}

	public function save()
	{
			$data['issue']           = $this->input->post('issue');
			$data['category_issue']   = $this->input->post('category_issue');
			$data['lokasi']          = $this->input->post('lokasi');
			$data['pic']             = $this->input->post('pic');
			$data['date']   = $this->input->post('date');
			$data['time']  = $this->input->post('time');
			$data['deskripsi_issue'] = $this->input->post('deskripsi_issue');
			$data['status']          = $this->input->post('status');
			$this->issue->save($data);
			$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Ditambahkan ! </div>");
			redirect('issue');
	}

	public function detail($id)
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('issue_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('issue_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->issue->get_by_id($id);
    $this->load_view('detail_view_template',$data);
	}

	public function set_nok($id)
 	{
		$data['status'] = 'NOK';
		$where['id'] = $id;
		$this->issue->update_status($where,$data);
		$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Status Berhasil Diubah ! </div>");
		redirect('issue');
	}

	public function set_ok($id)
 	{
		$data['status'] = 'OK';
		$where['id'] = $id;
		$this->issue->update_status($where,$data);
		$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Status Berhasil Diubah ! </div>");
		redirect('issue');
	}

	public function ubah($id)
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('issue_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('issue_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->issue->get_by_id($id);
    $this->load_view('ubah_view_template',$data);
	}

	public function update()
	{
			$where['id']						 = $this->input->post('id');
			$data['issue']           = $this->input->post('issue');
			$data['category_issue']   = $this->input->post('category_issue');
			$data['lokasi']          = $this->input->post('lokasi');
			$data['pic']             = $this->input->post('pic');
			$data['date']   = $this->input->post('date');
			$data['time']  = $this->input->post('time');
			$data['deskripsi_issue'] = $this->input->post('deskripsi_issue');
			//$data['status']          = 'NOK';
			$this->issue->update($where,$data);
			$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Diubah ! </div>");
			redirect('issue');
	}

}
