<section class="content">
    <h1 class="page-header"><i class="fa fa-check-square"></i> LIST AUDITOR</h1>
    <?php echo $this->session->flashdata('pesan'); ?>

    <div class="box box-info">
       <div class="box-body">
         <table id="tabel" class="table table-hover">
           <thead>
             <tr>
               <th>No</th>
               <th>Nama Audit</th>
               <th>Jenis Audit</th>
               <th>Status Pemenuhan</th>
               <th>PIC</th>
               <th>#</th>
             </tr>
           </thead>
           <tbody>
             <?php if (isset($list)) { $no=1; foreach($list as $row) { ?>
             <tr>
               <td><?php echo $no++; ?></td>
               <td><?php echo $row->nama_auditor; ?></td>
               <td><?php echo $row->jenis_auditor; ?></td>
               <td><?php echo $row->status_pemenuhan; ?></td>
               <td><?php echo $row->pic; ?></td>
               <td><a href="<?php echo base_url('auditor/detail/'.$row->id); ?>" class="btn btn-info">DETAIL</a></td>
             </tr>
             <?php } } ?>

           </tbody>
           <tfoot>
             <tr>
               <th>No</th>
               <th>Nama Audit</th>
               <th>Jenis Audit</th>
               <th>Status Pemenuhan</th>
               <th>PIC</th>
               <th>#</th>
             </tr>
           </tfoot>
         </table>
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
</section>
