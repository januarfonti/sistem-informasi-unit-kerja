<section class="content">
    <h1 class="page-header"><i class="fa fa-plus"></i> INPUT AUDIT</h1>
    <?php echo $this->session->flashdata('pesan'); ?>
    <?php echo form_open_multipart('auditor/save'); ?>
    <input type='hidden' name='sesi' class='sesi-from_galeri' value='<?php echo rand(0,100).rand(10,500).date('dym') ?>' >
    <input type='hidden' class='id_kliping' name="id_kliping" value='<?php echo $data['id']; ?>' >
    <?php if($foto!==false){ ?>

          <div class="form-group">

                  <?php
                    if (isset($foto)) { echo "<div class='well'><strong>RELATED DOCUMENT :</strong> <br/>";
                    foreach ($foto AS $gmb){
                      echo "<div id='$gmb[id]' class='col-md-12'>
                      <a style='color:#000;' href='".base_url()."uploads/document/".$gmb['name']."'>".$gmb['name']."</a>
                      <span class='delete_btn_container'>
                       <span class='fa fa-times delete_btn' id='$gmb[id]' data-toggle='tooltip' placement='top' title='Hapus foto'></span>
                     </span>
                     </div><br/>";
                   } echo "</div>";
                  }
                    ?>

           </div><!-- /form-group -->

            <?php
                              }
                            ?>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>NAMA AUDIT</label>
                    <input type="text" class="form-control" name="nama_auditor" value="<?php echo $data['nama_auditor']; ?>">
                </div>

                    <div class="form-group">
                        <label>JENIS AUDIT</label>
                        <select v-model="selected" class="form-control" name="jenis_auditor">
                            <option selected value="ICoFR">ICoFR</option>
                            <option value="Internal">Internal</option>
                            <option value="Eksternal">Eksternal</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>STATUS PEMENUHAN</label>
                        <select v-model="selected" class="form-control" name="status_pemenuhan">
                            <option selected value="In Progress">In Progress</option>
                            <option value="Closed">Closed</option>
                        </select>
                    </div>



            </div>
            <div class="col-md-6">

              <div class="form-group">
                  <label>TARGET PEMENUHAN</label>
                  <input type="text" class="form-control datepicker" name="target_pemenuhan" value="<?php echo $data['target_pemenuhan']; ?>">
              </div>
              <div class="form-group">
                  <label>LOKASI</label>
                  <input type="text" class="form-control" name="lokasi" value="<?php echo $data['lokasi']; ?>">
              </div>
              <div class="form-group">
                  <label>PIC</label>
                  <input type="text" class="form-control" name="pic" value="<?php echo $data['pic']; ?>">
              </div>


        </div>
      </div>
        <div class="form-group">
            <label>DESKRIPSI</label>
            <textarea name="deskripsi" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $data['deskripsi']; ?></textarea>
        </div>

        <div class="form-group">
              <label>UPLOAD DOCUMENT</label>

                  <div class="dropzone">
                   <div class="dz-message">
                     <h3> Click or Drag Document Here</h3>
                    </div>
                  </div>

          </div><!-- /form-group -->


        <button type="submit" class="btn btn-flat btn-block btn-lg">Submit</button>
    <?php echo form_close(); ?>

</section>
