<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auditor extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auditor_model','auditor');
	}

	public function index()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('auditor_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('auditor_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->auditor->get_document();
    $this->load_view('auditor_view_template',$data);
	}

	public function detail($id)
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('auditor_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('auditor_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->auditor->get_by_id($id);
		$data['file'] = $this->auditor->get_file($id);
    $this->load_view('detail_view_template',$data);
	}


	public function input($id=0)
    {
    	$id=abs($id);
			if($this->auditor->get_doc($id))
      {
				$cek_gambar=$this->auditor->ambil_gambar();
				$this->load->helper('url');
		    $this->set_template('backend');
		    $p_css = $this->load->view('auditor_view_css',array(),true);
		    $this->add_template_html_header($p_css);
		    $p_js = $this->load->view('auditor_view_js',array(),true);
		    $this->add_template_html_footer($p_js);
				$data['data']        = $this->auditor->hasil;
				$data['foto']        = ($cek_gambar==true)?$this->auditor->photos:false;
				$this->load_view('input_view_template',$data);
		  } else {
		  	echo "error";
		  }
		}



	public function proses_upload()
	{
      $config['upload_path']   = FCPATH.'/uploads/document/';
      $config['allowed_types'] = 'pdf|docx|doc|ppt|pptx|xls|xlsx';
      $this->load->library('upload',$config);
      if($this->upload->do_upload('userfile')){
					$id    = $this->input->post('id_kliping');
					$token = $this->input->post('token');
					$nama  = $this->upload->data('file_name');
					$sesi  = $this->input->post('sesi');
					if($id==0){
            	$this->db->insert('siuk_auditor_temp',array('document_name'=>$nama,'token'=>$token,'sesi_form'=>$sesi));
            } else {
            	$this->db->insert('siuk_auditor_file',array('id_auditor'=>$id,'name'=>$nama,'token'=>$token));
            }

	      }
    }


    public function remove_foto()
		{
    	$token=$this->input->post('token');
      $id_kliping=$this->input->post('id_kliping');

				if ($id_kliping == 0)
			 	{

				$kliping=$this->db->get_where('siuk_auditor_temp',array('token'=>$token));
				if($kliping->num_rows()>0){
						$hasil=$kliping->row();
						$document_name=$hasil->document_name;
						if(file_exists($file=FCPATH.'/uploads/document/'.$document_name)){
								unlink($file);
						}
						$this->db->delete('siuk_auditor_temp',array('token'=>$token));
				}
			}
			elseif ($id_kliping > 0) {
            $kliping=$this->db->get_where('siuk_auditor_file',array('token'=>$token));
            if($kliping->num_rows()>0){
                $hasil=$kliping->row();
                $document_name=$hasil->name;
                if(file_exists($file=FCPATH.'/upload/'.$document_name)){
                    unlink($file);
                }
                $this->db->delete('siuk_auditor_file',array('token'=>$token));
            }
        }

				echo "{}";
    }

		function delete_foto_galeri(){
		$id=$this->input->post("id");
		$search=$this->db->query("SELECT * FROM siuk_auditor_file WHERE id='$id'");
		if($search->num_rows()>0){
			$data=$search->row();
			$file=FCPATH."uploads/document/".$data->name;
			//$thumbnail=FCPATH."an-component/media/upload-galeri-thumbs/".$data->nama_foto;
			$query2=$this->db->query("DELETE FROM siuk_auditor_file WHERE id='$id' ");
			unlink($file);
			//unlink($thumbnail);
		}
		echo "ok";
	}

		public function save()
		{
			$id_kliping       = $this->input->post('id_kliping');
			$sesi             = $this->input->post('sesi');
			$nama_auditor     = $this->input->post('nama_auditor');
			$jenis_auditor    = $this->input->post('jenis_auditor');
			$status_pemenuhan = $this->input->post('status_pemenuhan');
			$target_pemenuhan = $this->input->post('target_pemenuhan');
			$lokasi           = $this->input->post('lokasi');
			$pic              = $this->input->post('pic');
			$deskripsi        = $this->input->post('deskripsi');

			if ($id_kliping==0)
			{

					$query=$this->db->query("INSERT INTO siuk_auditor (nama_auditor,jenis_auditor,status_pemenuhan,target_pemenuhan,lokasi,pic,deskripsi) VALUES ('$nama_auditor','$jenis_auditor','$status_pemenuhan','$target_pemenuhan','$lokasi','$pic','$deskripsi')");
						$new_id=$this->db->insert_id();

						$foto=$this->db->query("SELECT * FROM siuk_auditor_temp WHERE sesi_form='$sesi' ORDER BY id");
						if($foto->num_rows()>0)
						{
							foreach($foto->result_array() AS $data)
							{
								$this->db->query("INSERT INTO siuk_auditor_file (id_auditor,name) VALUES ('$new_id','$data[document_name]')");
							}
							$this->db->query("DELETE FROM siuk_auditor_temp WHERE sesi_form='$sesi'");
						}
						$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Disimpan ! </div>");
				}
				elseif($id_kliping>0) {
				$query=$this->db->query("UPDATE siuk_auditor SET
					nama_auditor='$nama_auditor',
					jenis_auditor='$jenis_auditor',
					status_pemenuhan='$status_pemenuhan',
					target_pemenuhan='$target_pemenuhan',
					lokasi='$lokasi',
					pic='$pic',
					deskripsi='$deskripsi'
					WHERE id='$id_kliping'
				");
				$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Diubah ! </div>");
			}

				redirect('auditor');
		}
}
