<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auditor_model extends CI_Model {

	var $table = 'siuk_auditor';
	//var $column = array('firstname','lastname','gender','address','dob'); //set column field database for order and search
	var $order = array('id' => 'desc'); // default order

	public $id;
	public $hasil;
	public $photos=array();

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_document()
	{
		$this->db->from($this->table);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_file($id)
	{
		$this->db->from('siuk_auditor_file');
		$this->db->where('id_auditor',$id);
		$query = $this->db->get();

		return $query->result();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_doc($id){
		$this->id=$id;

		if($id>0){
		$query=$this->db->query("SELECT * FROM siuk_auditor WHERE id='$id'");
			if($query->num_rows()>0){
				$row=$query->row();

				$this->hasil=array(
					"id"=>$row->id,
					"nama_auditor"=>$row->nama_auditor,
					"jenis_auditor"=>$row->jenis_auditor,
					"status_pemenuhan"=>$row->status_pemenuhan,
					"target_pemenuhan"=>$row->target_pemenuhan,
					"lokasi"=>$row->lokasi,
					"pic"=>$row->pic,
					"deskripsi"=>$row->deskripsi
					);

				return true;

			} else {
				return false;
			}
		} else {

				$this->hasil=array(
					"id"=>0,
					"nama_auditor"=>"",
					"jenis_auditor"=>"",
					"status_pemenuhan"=>"",
					"target_pemenuhan"=>"",
					"lokasi"=>"",
					"pic"=>"",
					"deskripsi"=>""
					);

			return true;

		}


	}

	function ambil_gambar(){
		if($this->id > 0){
			$query=$this->db->query("SELECT * FROM siuk_auditor_file WHERE id_auditor='".$this->id."' ORDER BY id DESC");
			if($query->num_rows()>0){

				$this->photos=$query->result_array();
			}
			return true;
		} else {
			return false;
		}
	}


}
