<?php if (isset($list)) { ?>
<section class="content">
    <h1 class="page-header">DETAIL DOCUMENT</h1>
      <div class="row">
          <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_1" data-toggle="tab">DESCRIPTION</a></li>
                      <li><a href="#tab_2" data-toggle="tab">RELATED DOCUMENT</a></li>
                  </ul>
                  <div class="tab-content">
                      <div class="tab-pane active" id="tab_1">
                        <div class="row">
                          <div class="col-md-6">
                          <p><strong>Nama Document : <?php echo $list->nama_document; ?></strong></p>
                          <p><strong>Jenis Document : <?php echo $list->jenis_document; ?></strong></p>
                          <p><strong>Lokasi : <?php echo $list->lokasi_document; ?></strong></p>
                          <p><strong>Status Document : <?php echo $list->status_document; ?></strong></p>
                          <p><strong>Masa Berlaku : <?php echo $list->masa_berlaku; ?></strong></p>
                          <p><strong>PIC : <?php echo $list->pic; ?></strong></p>
                          <?php echo $list->deskripsi; ?>
                        </div>
                        <div class="col-md-6">
                          <a class="pull-right btn btn-info" href="<?php echo base_url('document/input/'.$list->id); ?>">Ubah</a>
                        </div>
                      </div>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Document</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php if (isset($file)) { $no = 1; foreach ($file as $key) { ?>
                            <tr>
                              <td><?php echo $no++; ?></td>
                              <td><?php echo $key->name; ?></td>
                              <td><a href="<?php echo base_url('uploads/document/'.$key->name); ?>" class="btn btn-info btn-sm">View</a></td>
                            </tr>
                            <?php }$no++;} ?>
                          </tbody>
                        </table>
                      </div>
                      <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
              </div>
              <!-- nav-tabs-custom -->
          </div>
      </div>
</section>
  <?php } ?>
