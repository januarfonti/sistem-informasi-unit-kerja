<section class="content">
    <h1 class="page-header"><i class="fa fa-check-square"></i> LIST DOCUMENT</h1>
    <?php echo $this->session->flashdata('pesan'); ?>

    <div class="box box-info">
       <div class="box-body">
         <table id="tabel" class="table table-hover">
           <thead>
             <tr>
               <th>No</th>
               <th>Nama Document</th>
               <th>Jenis Document</th>
               <th>Status Document</th>
               <th>PIC</th>
               <th>#</th>
             </tr>
           </thead>
           <tbody>
             <?php if (isset($list)) { $no=1; foreach($list as $row) { ?>
             <tr>
               <td><?php echo $no++; ?></td>
               <td><?php echo $row->nama_document; ?></td>
               <td><?php echo $row->jenis_document; ?></td>
               <td><?php echo $row->status_document; ?></td>
               <td><?php echo $row->pic; ?></td>
               <td><a href="<?php echo base_url('document/detail/'.$row->id); ?>" class="btn btn-info">DETAIL</a></td>
             </tr>
             <?php } } ?>

           </tbody>
           <tfoot>
             <tr>
               <th>No</th>
               <th>Nama Document</th>
               <th>Jenis Document</th>
               <th>Status Document</th>
               <th>PIC</th>
               <th>#</th>
             </tr>
           </tfoot>
         </table>
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
</section>
