<section class="content">
    <h1 class="page-header"><i class="fa fa-plus"></i> INPUT DOCUMENT</h1>
    <?php echo $this->session->flashdata('pesan'); ?>
    <?php echo form_open_multipart('document/save'); ?>
    <input type='hidden' name='sesi' class='sesi-from_galeri' value='<?php echo rand(0,100).rand(10,500).date('dym') ?>' >
    <input type='hidden' class='id_kliping' name="id_kliping" value='<?php echo $data['id']; ?>' >
    <?php if($foto!==false){ ?>

          <div class="form-group">

                  <?php
                    if (isset($foto)) { echo "<div class='well'><strong>RELATED DOCUMENT :</strong> <br/>";
                    foreach ($foto AS $gmb){
                      echo "<div id='$gmb[id]' class='col-md-12'>
                      <a style='color:#fff;' href='".base_url()."uploads/document/".$gmb['name']."'>".$gmb['name']."</a>
                      <span class='delete_btn_container'>
                       <span class='fa fa-times delete_btn' id='$gmb[id]' data-toggle='tooltip' placement='top' title='Hapus foto'></span>
                     </span>
                     </div><br/>";
                   } echo "</div>";
                  }
                    ?>

           </div><!-- /form-group -->

            <?php
                              }
                            ?>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>NAMA DOCUMENT</label>
                    <input type="text" class="form-control" name="nama_document" value="<?php echo $data['nama_document']; ?>">
                </div>

                    <div class="form-group">
                        <label>JENIS DOCUMENT</label>
                        <select v-model="selected" class="form-control" name="jenis_document">
                            <option selected value="Policy">Arsip</option>
                            <option value="Pengadaan (PPL)">Pengadaan (PPL)</option>
                            <option value="Pengadaan">Pengadaan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>STATUS DOCUMENT</label>
                        <select v-model="selected" class="form-control" name="status_document">
                            <option selected value="Aktif">Aktif</option>
                            <option value="Tidak Aktif">Tidak Aktif</option>
                        </select>
                    </div>



            </div>
            <div class="col-md-6">

              <div class="form-group">
                  <label>MASA BERLAKU</label>
                  <input type="text" class="form-control datepicker" name="masa_berlaku" value="<?php echo $data['masa_berlaku']; ?>">
              </div>
              <div class="form-group">
                  <label>LOKASI DOCUMENT</label>
                  <input type="text" class="form-control" name="lokasi_document" value="<?php echo $data['lokasi_document']; ?>">
              </div>
              <div class="form-group">
                  <label>PIC</label>
                  <input type="text" class="form-control" name="pic" value="<?php echo $data['pic']; ?>">
              </div>


        </div>
      </div>
        <div class="form-group">
            <label>DESKRIPSI</label>
            <textarea name="deskripsi" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $data['deskripsi']; ?></textarea>
        </div>

        <div class="form-group">
              <label>UPLOAD DOCUMENT</label>

                  <div class="dropzone">
                   <div class="dz-message">
                     <h3> Click or Drag Document Here</h3>
                    </div>
                  </div>

          </div><!-- /form-group -->


        <button type="submit" class="btn btn-flat btn-block btn-lg">Submit</button>
    <?php echo form_close(); ?>

</section>
