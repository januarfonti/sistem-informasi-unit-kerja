<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_model extends CI_Model {

	var $table = 'siuk_document';
	//var $column = array('firstname','lastname','gender','address','dob'); //set column field database for order and search
	var $order = array('id' => 'desc'); // default order

	public $id;
	public $hasil;
	public $photos=array();

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_document()
	{
		$this->db->from($this->table);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_file($id)
	{
		$this->db->from('siuk_document_file');
		$this->db->where('id_document',$id);
		$query = $this->db->get();

		return $query->result();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_doc($id){
		$this->id=$id;

		if($id>0){
		$query=$this->db->query("SELECT * FROM siuk_document WHERE id='$id'");
			if($query->num_rows()>0){
				$row=$query->row();

				$this->hasil=array(
					"id"=>$row->id,
					"nama_document"=>$row->nama_document,
					"jenis_document"=>$row->jenis_document,
					"status_document"=>$row->status_document,
					"masa_berlaku"=>$row->masa_berlaku,
					"lokasi_document"=>$row->lokasi_document,
					"pic"=>$row->pic,
					"deskripsi"=>$row->deskripsi
					);

				return true;

			} else {
				return false;
			}
		} else {

				$this->hasil=array(
					"id"=>0,
					"nama_document"=>"",
					"jenis_document"=>"",
					"status_document"=>"",
					"masa_berlaku"=>"",
					"lokasi_document"=>"",
					"pic"=>"",
					"deskripsi"=>""
					);

			return true;

		}


	}

	function ambil_gambar(){
		if($this->id > 0){
			$query=$this->db->query("SELECT * FROM siuk_document_file WHERE id_document='".$this->id."' ORDER BY id DESC");
			if($query->num_rows()>0){

				$this->photos=$query->result_array();
			}
			return true;
		} else {
			return false;
		}
	}


}
