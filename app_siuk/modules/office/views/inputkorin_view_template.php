<section class="content">
    <h1 class="page-header"><i class="fa fa-plus"></i> INPUT KORIN</h1>
    <?php echo $this->session->flashdata('pesan'); ?>
    <?php echo form_open_multipart('office/savekorin'); ?>
    <input name="id_kliping" value="<?php echo $data['id']; ?>" type="hidden">

        <div class="form-group">
              <label>NAMA KORIN</label>
              <input type="text" class="form-control" name="nama_korin" value="<?php echo $data['nama_korin']; ?>">
          </div>

        <div class="form-group">
            <label>NO KORIN</label>
            <input type="text" class="form-control" name="no_korin" value="<?php echo $data['no_korin']; ?>">
        </div>

        <div class="form-group">
            <label>KEPADA</label>
            <input type="text" class="form-control" name="kepada" value="<?php echo $data['kepada']; ?>">
        </div>

        <div class="form-group">
            <label>DARI</label>
            <input type="text" class="form-control" name="dari" value="<?php echo $data['dari']; ?>">
        </div>

        <div class="form-group">
            <label>PERIHAL</label>
            <input type="text" class="form-control" name="perihal" value="<?php echo $data['perihal']; ?>">
        </div>



        <div class="form-group">
            <label>ISI KORIN</label>
            <textarea name="isi_korin" class="textarea" id="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $data['isi_korin']; ?></textarea>
        </div>

        <div class="form-group">
            <label>TANGGAL</label>
            <input type="text" class="form-control datepicker" name="tanggal" value="<?php echo $data['tanggal']; ?>">
        </div>

        <div class="form-group">
            <label>LOKASI</label>
            <input type="text" class="form-control" name="lokasi" value="<?php echo $data['lokasi']; ?>">
        </div>

        <div class="form-group">
            <label>PEJABAT</label>
            <input type="text" class="form-control" name="pejabat" value="<?php echo $data['pejabat']; ?>">
        </div>




        <button type="submit" class="btn btn-flat btn-lg">Submit</button>
    <?php echo form_close(); ?>

</section>
