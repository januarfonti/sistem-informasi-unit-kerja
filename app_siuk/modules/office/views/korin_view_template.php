<section class="content">
    <h1 class="page-header"><i class="fa fa-check-square"></i> LIST KORIN</h1>
    <?php echo $this->session->flashdata('pesan'); ?>

    <div class="box box-info">
       <div class="box-body">
         <table id="tabel" class="table table-hover">
           <thead>
             <tr>
               <th>No</th>
               <th>Nama Korin</th>
               <th>No Korin</th>
               <th>Tanggal</th>
               <th>Lokasi</th>
               <th>Aksi</th>
            </tr>
           </thead>
           <tbody>
             <?php if (isset($list)) { $no=1; foreach($list as $row) { ?>
             <tr>
               <td><?php echo $no++; ?></td>
               <td><?php echo $row->nama_korin; ?></td>
               <td><?php echo $row->no_korin; ?></td>
               <td><?php echo $row->tanggal; ?></td>
               <td><?php echo $row->lokasi; ?></td>
               <td><a class="btn btn-primary" href="<?php echo base_url('office/doc/'.$row->id); ?>">View</a>
                 &nbsp;
                 <a class="btn btn-primary" href="<?php echo base_url('office/inputkorin/'.$row->id); ?>">Ubah</a>
               </td>

             </tr>
             <?php } } ?>

           </tbody>
           <tfoot>
             <tr>
               <th>No</th>
               <th>Nama Korin</th>
               <th>No Korin</th>
               <th>Tanggal</th>
               <th>Lokasi</th>
               <th>Aksi</th>
             </tr>
           </tfoot>
         </table>
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
</section>
