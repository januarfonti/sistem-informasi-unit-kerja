<?php ob_start(); ?>

<?php if (isset($list)) { ?>
<table style="width: 100%;">
<tbody>
<tr style="height: 23px;">
<td style="width: 20%; height: 23px; font-weight: bold;" colspan="3">PT. SEMEN INDONESIA (Persero) Tbk.</td>
<td style="width: 20%; height: 23px;">&nbsp;</td>
<td style="width: 20%; height: 23px;">&nbsp;</td>
</tr>
<tr style="height: 23px;">
<td style="width: 20%; height: 23px;">&nbsp;</td>
<td style="width: 20%; height: 23px;">&nbsp;</td>
<td style="width: 20%; height: 23px;">&nbsp;</td>
<td style="width: 20%; height: 23px;">&nbsp;</td>
<td style="width: 20%; height: 23px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 20%;">&nbsp;</td>
<td style="padding-bottom:-22px; width: 20%; text-align: center; text-decoration: underline; font-weight: bold; font-size: 17px; text-transform:uppercase;" colspan="3"><?php echo $list->nama_korin; ?></td>
<td style="width: 20%;">&nbsp;</td>
</tr>
<tr>
<td style="width: 20%;">&nbsp;</td>
<td style="width: 20%; text-align: center;" colspan="3">Nomor : <?php echo $list->no_korin; ?></td>
<td style="width: 20%;">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- DivTable.com -->
<br><br>
<table style="width: 100%;">
<tbody>
<tr>
<td style="width: 15%; font-weight: bold;" valign="top">Kepada</td>
<td style="width: 1%;" valign="top">:</td>
<td style="width: 84%;" valign="top"><?php echo $list->kepada; ?></td>
</tr>

<tr>
<td style="width: 15%; font-weight: bold;" valign="top">Dari</td>
<td style="width: 1%;" valign="top">:</td>
<td style="width: 84%;" valign="top"><?php echo $list->dari; ?></td>
</tr>

<tr>
<td style="width: 15%; font-weight: bold;" valign="top">Perihal</td>
<td style="width: 1%;" valign="top">:</td>
<td style="width: 84%; text-decoration: underline; font-style:italic; font-weight: bold;" valign="top"><?php echo $list->perihal; ?></td>
</tr>

</tbody>
</table>
<!-- DivTable.com -->

<table style="width: 100%;">
<tbody>

<tr>
<td style="width: 15%;" valign="top"></td>
<td style="width: 1%;" valign="top"></td>
<td style="width: 84%;"><div style="text-align:justify;"><?php echo $list->isi_korin; ?></div></td>
</tr>

</tbody>
</table>
<!-- DivTable.com -->
<br><br>
<table style="width: 100%;">
<tbody>

<tr>
<td style="width: 20%;" valign="top"></td>
<td style="width: 20%;" valign="top"></td>
<td style="width: 20%;" valign="top"></td>
<td style="width: 25%; text-align: center;" valign="top"><?php echo $list->lokasi.", ".$list->tanggal; ?></td>
<td style="width: 5%;" valign="top"></td>
</tr>

<tr>
<td style="width: 20%;" valign="top"></td>
<td style="width: 20%;" valign="top"></td>
<td style="width: 20%;" valign="top"></td>
<td style="width: 25%; text-align: center;" valign="top">Departement of strategic ICT</td>
<td style="width: 5%;" valign="top"></td>
</tr>

</tbody>
</table>
<!-- DivTable.com -->

<br><br><br><br>
<table style="width: 100%;">
<tbody>

<tr>
<td style="width: 20%;" valign="top"></td>
<td style="width: 20%;" valign="top"></td>
<td style="width: 20%;" valign="top"></td>
<td style="width: 25%; text-align: center; font-weight: bold; text-decoration:underline;" valign="top"><?php echo $list->pejabat; ?></td>
<td style="width: 5%;" valign="top"></td>
</tr>

<tr>
<td style="width: 20%;" valign="top"></td>
<td style="width: 20%;" valign="top"></td>
<td style="width: 20%;" valign="top"></td>
<td style="width: 25%; text-align: center;" valign="top">GM</td>
<td style="width: 5%;" valign="top"></td>
</tr>

</tbody>
</table>
<!-- DivTable.com -->

<?php } ?>
<?php

//$filename=  $kliping->nama_koran."_".$kliping->nama_klasifikasi."_".$kliping->tanggal.".pdf"; //Set Nama File
$filename=  $list->nama_korin.".pdf"; //Set Nama File
$content = ob_get_clean();

 try
 {
  $html2pdf = new HTML2PDF('P','A4','fr','UTF-8');
  $html2pdf->writeHTML($content);
  $html2pdf->pdf->includeJS('print(TRUE)');
  $html2pdf->Output($filename);
 }
 catch(HTML2PDF_exception $e) { echo $e; }

?>
