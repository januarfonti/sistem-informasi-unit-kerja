<?php if (isset($file)) { ?>
<section class="content">
    <h1 class="page-header"><i class="fa fa-book"></i> DOCUMENT NOTULEN</h1>
      <div class="row">
          <div class="col-md-12">
          
          <div class="box box-info">
       <div class="box-body">
          <table class="table table-striped">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Document</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php if (isset($file)) { $no = 1; foreach ($file as $key) { ?>
                            <tr>
                              <td><?php echo $no++; ?></td>
                              <td><?php echo $key->name; ?></td>
                              <td><a href="<?php echo base_url('uploads/document/'.$key->name); ?>" class="btn btn-info btn-sm">View</a></td>
                            </tr>
                            <?php }$no++;} ?>
                          </tbody>
                        </table>
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
      
          </div>
      </div>
</section>
  <?php } ?>