<?php ob_start(); ?>

<?php if (isset($list)) { ?>
<span style="font-weight:bold; font-size:18px;">Minutes of Meeting</span>
<hr><br>
  <table style="width: 100%;" style="background:#f4f4f4; width:100%;">
  <tbody>
  <tr>
  <td style="width: 20%; font-weight: bold; color:#D64541;" valign="top">Meeting Title</td>
  <td style="width: 1%;" valign="top">:</td>
  <td style="width: 79%; font-weight: bold;" valign="top"><?php echo $list->nama_rapat; ?></td>
  </tr>

  <tr>
  <td style="width: 20%; font-weight: bold; color:#D64541;" valign="top">Date</td>
  <td style="width: 1%;" valign="top">:</td>
  <td style="width: 79%;" valign="top"><?php echo $list->waktu; ?></td>
  </tr>

  <tr>
  <td style="width: 20%; font-weight: bold; color:#D64541;" valign="top">Time</td>
  <td style="width: 1%;" valign="top">:</td>
  <td style="width: 79%;" valign="top"><?php echo $list->jam; ?></td>
  </tr>

  <tr>
  <td style="width: 20%; font-weight: bold; color:#D64541;" valign="top">Place</td>
  <td style="width: 1%;" valign="top">:</td>
  <td style="width: 79%;" valign="top"><?php echo $list->lokasi; ?></td>
  </tr>

  <tr>
  <td style="width: 20%; font-weight: bold; color:#D64541;" valign="top">Chair Person</td>
  <td style="width: 1%;" valign="top">:</td>
  <td style="width: 79%;" valign="top"><?php echo $list->chair_person; ?></td>
  </tr>

  <tr>
  <td style="width: 20%; font-weight: bold; color:#D64541;" valign="top">In Attendance</td>
  <td style="width: 1%;" valign="top">:</td>
  <td style="width: 79%;" valign="top"><span style="margin-top:-13px;"><?php echo $list->in_attendance; ?></span></td>
  </tr>

  <tr>
  <td style="width: 20%; font-weight: bold; color:#D64541;" valign="top">Agenda</td>
  <td style="width: 1%;" valign="top">:</td>
  <td style="width: 79%;" valign="top"><?php echo $list->agenda; ?></td>
  </tr>

  <tr>
  <td style="width: 20%; font-weight: bold; color:#D64541;" valign="top">Submitted by</td>
  <td style="width: 1%;" valign="top">:</td>
  <td style="width: 79%;" valign="top"><?php echo $list->submitted; ?></td>
  </tr>

  </tbody>
  </table>
  <!-- DivTable.com -->
  <p style="font-weight: bold;">Conclusions : </p>
  <?php echo $list->kesimpulan; ?>

  <p style="font-weight: bold;">Summary : </p>
  <?php echo $list->ringkasan; ?>

  <p style="font-weight: bold;">Action Plan : </p>
  <?php echo $list->action_plan; ?>

<?php } ?>
<?php

//$filename=  $kliping->nama_koran."_".$kliping->nama_klasifikasi."_".$kliping->tanggal.".pdf"; //Set Nama File
$filename=  $list->nama_rapat.".pdf"; //Set Nama File
$content = ob_get_clean();

 try
 {
  $html2pdf = new HTML2PDF('P','A4','fr','UTF-8');
  $html2pdf->writeHTML($content);
  $html2pdf->pdf->includeJS('print(TRUE)');
  $html2pdf->Output($filename);
 }
 catch(HTML2PDF_exception $e) { echo $e; }

?>
