<section class="content">
    <h1 class="page-header"><i class="fa fa-plus"></i> INPUT NOTULEN</h1>
    <?php echo $this->session->flashdata('pesan'); ?>
    <?php echo form_open_multipart('office/savenotulen'); ?>
    <input type='hidden' name='sesi' class='sesi-from_galeri' value='<?php echo rand(0,100).rand(10,500).date('dym') ?>' >
    <input type='hidden' class='id_kliping' name="id_kliping" value='<?php echo $data['id']; ?>' >

          <div class="form-group">
              <label>NAMA RAPAT</label>
              <input type="text" class="form-control" name="nama_rapat" value="<?php echo $data['nama_rapat']; ?>">
          </div>

        <div class="form-group">
            <label>TANGGAL</label>
            <input type="text" class="form-control datepicker" name="waktu" value="<?php echo $data['waktu']; ?>">
        </div>

        <div class="form-group">
            <label>JAM</label>
            <input type="text" class="form-control" name="jam" value="<?php echo $data['jam']; ?>">
        </div>

        <div class="row">

            <div class="col-md-6">

              <div class="form-group">
                  <label>LOKASI</label>
                  <input type="text" class="form-control" name="lokasi" value="<?php echo $data['lokasi']; ?>">
              </div>

              <div class="form-group">
                  <label>AGENDA</label>
                  <input type="text" class="form-control" name="agenda" value="<?php echo $data['agenda']; ?>">
              </div>



            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label>CHAIR PERSON</label>
                  <input type="text" class="form-control" name="chair_person" value="<?php echo $data['chair_person']; ?>">
              </div>

              <div class="form-group">
                  <label>SUBMITTED</label>
                  <input type="text" class="form-control" name="submitted" value="<?php echo $data['submitted']; ?>">
              </div>

            </div>
      </div>

      <div class="form-group">
          <label>IN ATTENDANCE</label>
          <textarea name="in_attendance" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $data['in_attendance']; ?></textarea>
      </div>

        <div class="form-group">
            <label>KESIMPULAN</label>
            <textarea name="kesimpulan" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $data['kesimpulan']; ?></textarea>
        </div>

        <div class="form-group">
            <label>RINGKASAN</label>
            <textarea name="ringkasan" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $data['ringkasan']; ?></textarea>
        </div>

        <div class="form-group">
            <label>ACTION PLAN</label>
            <textarea name="action_plan" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $data['action_plan']; ?></textarea>
        </div>


        <button type="submit" class="btn btn-flat btn-lg btn-block">Submit</button>
    <?php echo form_close(); ?>

</section>
