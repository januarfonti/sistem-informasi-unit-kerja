<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Office_model extends CI_Model {

	var $table = 'siuk_notulen';
	//var $column = array('firstname','lastname','gender','address','dob'); //set column field database for order and search
	var $order = array('id' => 'desc'); // default order

	public $id;
	public $hasil;
	public $hasil_korin;
	public $photos=array();

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_notulen()
	{
		$this->db->from($this->table);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_korin()
	{
		$this->db->from('siuk_korin');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from('siuk_korin');
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_by_id_notulen($id)
	{
		$this->db->from('siuk_notulen');
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_file($id)
	{
		$this->db->from('siuk_notulen_file');
		$this->db->where('id_notulen',$id);
		$query = $this->db->get();

		return $query->result();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_doc($id){
		$this->id=$id;

		if($id>0){
		$query=$this->db->query("SELECT * FROM siuk_notulen WHERE id='$id'");
			if($query->num_rows()>0){
				$row=$query->row();

				$this->hasil=array(
					"id"=>$row->id,
					"nama_rapat"=>$row->nama_rapat,
					"waktu"=>$row->waktu,
					"jam"=>$row->jam,
					"lokasi"=>$row->lokasi,
					"agenda"=>$row->agenda,
					"chair_person"=>$row->chair_person,
					"submitted"=>$row->submitted,
					"in_attendance"=>$row->in_attendance,
					"kesimpulan"=>$row->kesimpulan,
					"ringkasan"=>$row->ringkasan,
					"action_plan"=>$row->action_plan
					);

				return true;

			} else {
				return false;
			}
		} else {

				$this->hasil=array(
					"id"=>0,
					"nama_rapat"=>"",
					"waktu"=>"",
					"jam"=>"",
					"lokasi"=>"",
					"agenda"=>"",
					"chair_person"=>"",
					"submitted"=>"",
					"in_attendance"=>"",
					"kesimpulan"=>"",
					"ringkasan"=>"",
					"action_plan"=>""
					);

			return true;

		}


	}

	function ambil_gambar(){
		if($this->id > 0){
			$query=$this->db->query("SELECT * FROM siuk_notulen_file WHERE id_notulen='".$this->id."' ORDER BY id DESC");
			if($query->num_rows()>0){

				$this->photos=$query->result_array();
			}
			return true;
		} else {
			return false;
		}
	}

	function get_doc_korin($id){
		$this->id=$id;

		if($id>0){
		$query=$this->db->query("SELECT * FROM siuk_korin WHERE id='$id'");
			if($query->num_rows()>0){
				$row=$query->row();

				$this->hasil_korin=array(
					"id"=>$row->id,
					"nama_korin"=>$row->nama_korin,
					"no_korin"=>$row->no_korin,
					"kepada"=>$row->kepada,
					"dari"=>$row->dari,
					"perihal"=>$row->perihal,
					"isi_korin"=>$row->isi_korin,
					"tanggal"=>$row->tanggal,
					"lokasi"=>$row->lokasi,
					"pejabat"=>$row->pejabat
					);

				return true;

			} else {
				return false;
			}
		} else {

				$this->hasil_korin=array(
					"id"=>0,
					"nama_korin"=>"",
					"no_korin"=>"",
					"kepada"=>"",
					"dari"=>"",
					"perihal"=>"",
					"isi_korin"=>"",
					"tanggal"=>"",
					"lokasi"=>"",
					"pejabat"=>""
					);

			return true;

		}


	}


}
