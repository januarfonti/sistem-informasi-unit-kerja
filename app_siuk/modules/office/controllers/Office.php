<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Office extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Office_model','office');
	}

	public function index()
	{
		redirect('office/notulen');
	}

	public function notulen()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('office_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('office_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->office->get_notulen();
    $this->load_view('notulen_view_template',$data);
	}

	public function document($id)
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('office_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('office_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['file'] = $this->office->get_file($id);
    $this->load_view('document_view_template',$data);
	}

	public function korin()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('office_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('office_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->office->get_korin();
    $this->load_view('korin_view_template',$data);
	}


	public function inputnotulen($id=0)
    {
    	$id=abs($id);
			if($this->office->get_doc($id))
      {
				$cek_gambar=$this->office->ambil_gambar();
				$this->load->helper('url');
		    $this->set_template('backend');
		    $p_css = $this->load->view('office_view_css',array(),true);
		    $this->add_template_html_header($p_css);
		    $p_js = $this->load->view('office_view_js',array(),true);
		    $this->add_template_html_footer($p_js);
				$data['data']        = $this->office->hasil;
				$data['foto']        = ($cek_gambar==true)?$this->office->photos:false;
				$this->load_view('inputnotulen_view_template',$data);
		  } else {
		  	echo "error";
		  }
		}

		public function inputkorin($id=0)
	    {
	    	$id=abs($id);
				if($this->office->get_doc_korin($id))
	      {
					//$cek_gambar=$this->office->ambil_gambar();
					$this->load->helper('url');
			    $this->set_template('backend');
			    $p_css = $this->load->view('office_view_css',array(),true);
			    $this->add_template_html_header($p_css);
			    $p_js = $this->load->view('office_view_js',array(),true);
			    $this->add_template_html_footer($p_js);
					$data['data']        = $this->office->hasil_korin;
					//$data['foto']        = ($cek_gambar==true)?$this->office->photos:false;
					$this->load_view('inputkorin_view_template',$data);
			  } else {
			  	echo "error";
			  }
			}




	public function proses_upload()
	{
      $config['upload_path']   = FCPATH.'/uploads/document/';
      $config['allowed_types'] = 'pdf|docx|doc|ppt|pptx|xls|xlsx';
      $this->load->library('upload',$config);
      if($this->upload->do_upload('userfile')){
					$id    = $this->input->post('id_kliping');
					$token = $this->input->post('token');
					$nama  = $this->upload->data('file_name');
					$sesi  = $this->input->post('sesi');
					if($id==0){
            	$this->db->insert('siuk_notulen_temp',array('document_name'=>$nama,'token'=>$token,'sesi_form'=>$sesi));
            } else {
            	$this->db->insert('siuk_notulen_file',array('id_notulen'=>$id,'name'=>$nama,'token'=>$token));
            }

	      }
    }


    public function remove_foto()
		{
    	$token=$this->input->post('token');
      $id_kliping=$this->input->post('id_kliping');

				if ($id_kliping == 0)
			 	{

				$kliping=$this->db->get_where('siuk_notulen_temp',array('token'=>$token));
				if($kliping->num_rows()>0){
						$hasil=$kliping->row();
						$document_name=$hasil->document_name;
						if(file_exists($file=FCPATH.'/uploads/document/'.$document_name)){
								unlink($file);
						}
						$this->db->delete('siuk_notulen_temp',array('token'=>$token));
				}
			}
			elseif ($id_kliping > 0) {
            $kliping=$this->db->get_where('siuk_notulen_file',array('token'=>$token));
            if($kliping->num_rows()>0){
                $hasil=$kliping->row();
                $document_name=$hasil->name;
                if(file_exists($file=FCPATH.'/upload/'.$document_name)){
                    unlink($file);
                }
                $this->db->delete('siuk_notulen_file',array('token'=>$token));
            }
        }

				echo "{}";
    }

		function delete_foto_galeri(){
		$id=$this->input->post("id");
		$search=$this->db->query("SELECT * FROM siuk_notulen_file WHERE id='$id'");
		if($search->num_rows()>0){
			$data=$search->row();
			$file=FCPATH."uploads/document/".$data->name;
			//$thumbnail=FCPATH."an-component/media/upload-galeri-thumbs/".$data->nama_foto;
			$query2=$this->db->query("DELETE FROM siuk_notulen_file WHERE id='$id' ");
			unlink($file);
			//unlink($thumbnail);
		}
		echo "ok";
	}

	public function savekorin()
	{
		$id_kliping   = $this->input->post('id_kliping');

		$data['nama_korin'] = $this->input->post('nama_korin');
		$data['no_korin']   = $this->input->post('no_korin');
		$data['kepada']     = $this->input->post('kepada');
		$data['dari']       = $this->input->post('dari');
		$data['perihal']    = $this->input->post('perihal');
		$data['isi_korin']  = $this->input->post('isi_korin');
		$data['tanggal']    = $this->input->post('tanggal');
		$data['lokasi']     = $this->input->post('lokasi');
		$data['pejabat']    = $this->input->post('pejabat');

		if ($id_kliping==0)
		{
				$this->db->insert('siuk_korin',$data);
				$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Disimpan ! </div>");
		}
		elseif($id_kliping>0)
		{
				$this->db->where('id', $id_kliping);
				$this->db->update('siuk_korin', $data);
				$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Diubah ! </div>");
		}
		redirect('office/korin');
	}

		public function savenotulen()
		{
			$id_kliping   = $this->input->post('id_kliping');
			$sesi         = $this->input->post('sesi');
			$nama_rapat   = $this->input->post('nama_rapat');
			$waktu        = $this->input->post('waktu');
			$jam					= $this->input->post('jam');
			$lokasi       = $this->input->post('lokasi');
			$agenda       = $this->input->post('agenda');
			$chair_person = $this->input->post('chair_person');
			$submitted    = $this->input->post('submitted');
			$in_attendance = $this->input->post('in_attendance');
			$kesimpulan   = $this->input->post('kesimpulan');
			$ringkasan    = $this->input->post('ringkasan');
			$action_plan  = $this->input->post('action_plan');


			if ($id_kliping==0)
			{

					$query=$this->db->query("INSERT INTO siuk_notulen (nama_rapat,waktu,jam,lokasi,agenda,chair_person,submitted,in_attendance,kesimpulan,ringkasan,action_plan) VALUES ('$nama_rapat','$waktu','$jam','$lokasi','$agenda','$chair_person','$submitted','$in_attendance','$kesimpulan','$ringkasan','$action_plan')");
					$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Disimpan ! </div>");
				}
				elseif($id_kliping>0) {
				$query=$this->db->query("UPDATE siuk_notulen SET
					nama_rapat='$nama_rapat',
					waktu='$waktu',
					jam='$jam',
					lokasi='$lokasi',
					agenda='$agenda',
					chair_person='$chair_person',
					submitted='$submitted',
					in_attendance='$in_attendance',
					kesimpulan='$kesimpulan',
					ringkasan='$ringkasan',
					action_plan='$action_plan'
					WHERE id='$id_kliping'
				");
				$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Diubah ! </div>");
			}

				redirect('office');
		}

		public function doc($id)
		{
			$data['list'] = $this->office->get_by_id($id);
	    $this->load->view('doc',$data);
		}

		public function docnotulen($id)
		{
			$data['list'] = $this->office->get_by_id_notulen($id);
	    $this->load->view('doc_notulen',$data);
		}



}
