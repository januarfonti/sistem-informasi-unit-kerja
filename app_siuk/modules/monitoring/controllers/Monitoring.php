<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Monitoring_model','monitoring');
	}

	public function index()
	{
		redirect('monitoring/performance');
	}

	public function performance()
	{
		$this->load->helper('url');
    $this->set_template('backend');
		$p_css = $this->load->view('monitoring_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('monitoring_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->monitoring->get_performance();
		$data['judul'] = 'Performance';
    $this->load_view('index_view_template',$data);
	}

	public function servicemanagement()
	{
		$this->load->helper('url');
    $this->set_template('backend');
		$p_css = $this->load->view('monitoring_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('monitoring_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->monitoring->get_service();
		$data['judul'] = 'Service Management';
		$this->load_view('index_view_template',$data);
	}

	public function governance()
	{
		$this->load->helper('url');
    $this->set_template('backend');
		$p_css = $this->load->view('monitoring_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('monitoring_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->monitoring->get_governance();
		$data['judul'] = 'Governance';
		$this->load_view('index_view_template',$data);
	}

	public function opco()
	{
		$this->load->helper('url');
    $this->set_template('backend');
		$p_css = $this->load->view('monitoring_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('monitoring_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->monitoring->get_opco();
		$data['judul'] = 'OPCO';
		$this->load_view('index_view_template',$data);
	}

	public function mm()
	{
		$this->load->helper('url');
    $this->set_template('backend');
		$p_css = $this->load->view('monitoring_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('monitoring_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->monitoring->get_mm();
		$data['judul'] = 'MM';
		$this->load_view('index_view_template',$data);
	}

	public function fico()
	{
		$this->load->helper('url');
    $this->set_template('backend');
		$p_css = $this->load->view('monitoring_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('monitoring_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->monitoring->get_fico();
		$data['judul'] = 'FICO';
		$this->load_view('index_view_template',$data);
	}

	public function pm()
	{
		$this->load->helper('url');
    $this->set_template('backend');
		$p_css = $this->load->view('monitoring_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('monitoring_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->monitoring->get_pm();
		$data['judul'] = 'PM/PS/PP';
		$this->load_view('index_view_template',$data);
	}

	public function sd()
	{
		$this->load->helper('url');
    $this->set_template('backend');
		$p_css = $this->load->view('monitoring_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('monitoring_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->monitoring->get_sd();
		$data['judul'] = 'SD';
		$this->load_view('index_view_template',$data);
	}

	public function grafik($id)
	{
		$this->load->helper('url');
    $this->set_template('backend');
		$bulan            = $this->monitoring->get_bulan($id);
		$datagrafik       = $this->monitoring->get_datagrafik($id);
		$grafik['bulan']  = json_encode(array_column($bulan,'bulan'));
		$grafik['actual'] = json_encode(array_column($datagrafik,'actual'));
		$grafik['target'] = json_encode(array_column($datagrafik,'target'));
    $p_css          	= $this->load->view('monitoring_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('monitoring_view_js',$grafik,true);
    $this->add_template_html_footer($p_js);
		$data['list']   = $this->monitoring->get_performance();
		$data['grafik'] = $this->monitoring->get_tabelgrafik($id);
		$data['detail'] = $this->monitoring->get_by_id($id);
		//print_r($datagrafik);
    $this->load_view('grafikperformance_view_template',$data);
	}

	public function document($id)
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('monitoring_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('monitoring_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['file'] = $this->monitoring->get_file($id);
    $this->load_view('document_view_template',$data);
	}


	public function input($id=0)
    {
    	$id=abs($id);
			if($this->monitoring->get_doc($id))
      {
				$cek_gambar=$this->monitoring->ambil_gambar();
				$this->load->helper('url');
		    $this->set_template('backend');
		    $p_css = $this->load->view('monitoring_view_css',array(),true);
		    $this->add_template_html_header($p_css);
		    $p_js = $this->load->view('monitoring_view_js',array(),true);
		    $this->add_template_html_footer($p_js);
				$data['data']        = $this->monitoring->hasil;
				$data['foto']        = ($cek_gambar==true)?$this->monitoring->photos:false;
				$this->load_view('input_view_template',$data);
		  } else {
		  	echo "error";
		  }
		}



	public function proses_upload()
	{
      $config['upload_path']   = FCPATH.'/uploads/document/';
      $config['allowed_types'] = 'pdf|docx|doc|ppt|pptx|xls|xlsx';
      $this->load->library('upload',$config);
      if($this->upload->do_upload('userfile')){
					$id    = $this->input->post('id_kliping');
					$token = $this->input->post('token');
					$nama  = $this->upload->data('file_name');
					$sesi  = $this->input->post('sesi');
					if($id==0){
            	$this->db->insert('siuk_monitoring_temp',array('document_name'=>$nama,'token'=>$token,'sesi_form'=>$sesi));
            } else {
            	$this->db->insert('siuk_monitoring_file',array('id_monitoring'=>$id,'name'=>$nama,'token'=>$token));
            }

	      }
    }


    public function remove_foto()
		{
    	$token=$this->input->post('token');
      $id_kliping=$this->input->post('id_kliping');

				if ($id_kliping == 0)
			 	{

				$kliping=$this->db->get_where('siuk_monitoring_temp',array('token'=>$token));
				if($kliping->num_rows()>0){
						$hasil=$kliping->row();
						$document_name=$hasil->document_name;
						if(file_exists($file=FCPATH.'/uploads/document/'.$document_name)){
								unlink($file);
						}
						$this->db->delete('siuk_monitoring_temp',array('token'=>$token));
				}
			}
			elseif ($id_kliping > 0) {
            $kliping=$this->db->get_where('siuk_monitoring_file',array('token'=>$token));
            if($kliping->num_rows()>0){
                $hasil=$kliping->row();
                $document_name=$hasil->name;
                if(file_exists($file=FCPATH.'/upload/'.$document_name)){
                    unlink($file);
                }
                $this->db->delete('siuk_monitoring_file',array('token'=>$token));
            }
        }

				echo "{}";
    }

		function delete_foto_galeri(){
		$id=$this->input->post("id");
		$search=$this->db->query("SELECT * FROM siuk_monitoring_file WHERE id='$id'");
		if($search->num_rows()>0){
			$data=$search->row();
			$file=FCPATH."uploads/document/".$data->name;
			//$thumbnail=FCPATH."an-component/media/upload-galeri-thumbs/".$data->nama_foto;
			$query2=$this->db->query("DELETE FROM siuk_monitoring_file WHERE id='$id' ");
			unlink($file);
			//unlink($thumbnail);
		}
		echo "ok";
	}

		public function save()
		{
			$id_kliping         = $this->input->post('id_kliping');
			$sesi               = $this->input->post('sesi');
			$data['nama_item']  = $this->input->post('nama_item');
			$data['kategori']		= $this->input->post('kategori');
			$data['type']       = $this->input->post('type');
			$data['jenis']      = $this->input->post('jenis');
			$data['keterangan'] = $this->input->post('keterangan');
			$query              = $this->db->insert('siuk_monitoring',$data);
			$new_id             = $this->db->insert_id();
			$bulan              = $this->input->post('bulan');
			$result             = array();
			foreach ($bulan as $key => $val) {
				$result[] = array (
					"bulan" => $_POST['bulan'][$key],
					"actual" => $_POST['actual'][$key],
					"target" => $_POST['target'][$key],
					"id_monitoring" => $new_id
				);
			}
			$this->db->insert_batch('siuk_monitoring_data',$result);


			$foto=$this->db->query("SELECT * FROM siuk_monitoring_temp WHERE sesi_form='$sesi' ORDER BY id");
			if($foto->num_rows()>0)
			{
				foreach($foto->result_array() AS $data)
				{
					$this->db->query("INSERT INTO siuk_monitoring_file (id_monitoring,name) VALUES ('$new_id','$data[document_name]')");
				}
				$this->db->query("DELETE FROM siuk_monitoring_temp WHERE sesi_form='$sesi'");
			}
			$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Disimpan ! </div>");

			redirect('monitoring');

		}

}
