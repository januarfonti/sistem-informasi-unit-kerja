<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring_model extends CI_Model {

	var $table = 'siuk_monitoring';
	//var $column = array('firstname','lastname','gender','address','dob'); //set column field database for order and search
	var $order = array('id' => 'desc'); // default order

	public $id;
	public $hasil;
	public $photos=array();

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_performance()
	{
		$this->db->from($this->table);
		$this->db->where('type','Performance');
		$this->db->where('kategori','Performance & Governance');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_service()
	{
		$this->db->from($this->table);
		$this->db->where('type','Service Management');
		$this->db->where('kategori','Performance & Governance');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_governance()
	{
		$this->db->from($this->table);
		$this->db->where('type','Governance');
		$this->db->where('kategori','Performance & Governance');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_opco()
	{
		$this->db->from($this->table);
		$this->db->where('type','OPCO');
		$this->db->where('kategori','Performance & Governance');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_mm()
	{
		$this->db->from($this->table);
		$this->db->where('type','MM');
		$this->db->where('kategori','Demand Management');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_fico()
	{
		$this->db->from($this->table);
		$this->db->where('type','FICO');
		$this->db->where('kategori','Demand Management');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_pm()
	{
		$this->db->from($this->table);
		$this->db->where('type','PM/PS/PP');
		$this->db->where('kategori','Demand Management');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_sd()
	{
		$this->db->from($this->table);
		$this->db->where('type','SD');
		$this->db->where('kategori','Demand Management');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_bulan($id)
	{
			$this->db->select('bulan');
			$this->db->from('siuk_monitoring_data');
			$this->db->where('id_monitoring',$id);
			$query = $this->db->get();
			return $query->result_array();
	}

	public function get_datagrafik($id)
	{
			$this->db->select('actual,target');
			$this->db->from('siuk_monitoring_data');
			$this->db->where('id_monitoring',$id);
			$query = $this->db->get();
			return $query->result_array();
	}

	public function get_tabelgrafik($id)
	{
		$this->db->select('*');
		$this->db->from('siuk_monitoring_data');
		$this->db->where('id_monitoring',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_file($id)
	{
		$this->db->from('siuk_monitoring_file');
		$this->db->where('id_monitoring',$id);
		$query = $this->db->get();

		return $query->result();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_doc($id){
		$this->id=$id;

		if($id>0){
		$query=$this->db->query("SELECT * FROM siuk_auditor WHERE id='$id'");
			if($query->num_rows()>0){
				$row=$query->row();

				$this->hasil=array(
					"id"=>$row->id,
					"nama_auditor"=>$row->nama_auditor,
					"jenis_auditor"=>$row->jenis_auditor,
					"status_pemenuhan"=>$row->status_pemenuhan,
					"target_pemenuhan"=>$row->target_pemenuhan,
					"lokasi"=>$row->lokasi,
					"pic"=>$row->pic,
					"deskripsi"=>$row->deskripsi
					);

				return true;

			} else {
				return false;
			}
		} else {

				$this->hasil=array(
					"id"=>0,
					"nama_auditor"=>"",
					"jenis_auditor"=>"",
					"status_pemenuhan"=>"",
					"target_pemenuhan"=>"",
					"lokasi"=>"",
					"pic"=>"",
					"deskripsi"=>""
					);

			return true;

		}


	}

	function ambil_gambar(){
		if($this->id > 0){
			$query=$this->db->query("SELECT * FROM siuk_auditor_file WHERE id_auditor='".$this->id."' ORDER BY id DESC");
			if($query->num_rows()>0){

				$this->photos=$query->result_array();
			}
			return true;
		} else {
			return false;
		}
	}


}
