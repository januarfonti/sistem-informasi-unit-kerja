<section class="content">
    <h1 class="page-header"><i class="fa fa-plus"></i> INPUT DATA MONITORING</h1>
    <?php echo $this->session->flashdata('pesan'); ?>
    <?php echo form_open_multipart('monitoring/save'); ?>
    <input type='hidden' name='sesi' class='sesi-from_galeri' value='<?php echo rand(0,100).rand(10,500).date('dym') ?>' >
    <input type='hidden' class='id_kliping' name="id_kliping" value='0' >
    <?php if($foto!==false){ ?>

          <div class="form-group">

                  <?php
                    if (isset($foto)) { echo "<div class='well'><strong>RELATED DOCUMENT :</strong> <br/>";
                    foreach ($foto AS $gmb){
                      echo "<div id='$gmb[id]' class='col-md-12'>
                      <a style='color:#000;' href='".base_url()."uploads/document/".$gmb['name']."'>".$gmb['name']."</a>
                      <span class='delete_btn_container'>
                       <span class='fa fa-times delete_btn' id='$gmb[id]' data-toggle='tooltip' placement='top' title='Hapus foto'></span>
                     </span>
                     </div><br/>";
                   } echo "</div>";
                  }
                    ?>

           </div><!-- /form-group -->

            <?php
                              }
                            ?>
        <div class="form-group">
          <label>NAMA ITEM</label>
          <input type="text" class="form-control" name="nama_item">
        </div>

        <div id='app'>


          <div class="form-group">
              <label>CATEGORY</label>
              <select v-model="category" class="form-control" name="kategori">
                  <option selected value="Performance & Governance">Performance & Governance</option>
                  <option value="Demand Management">Demand Management</option>
              </select>
          </div>

          <div v-if="category == 'Performance & Governance'">

            <div class="form-group">
                <label>TYPE</label>
                <select v-model="selected" class="form-control" name="type">
                    <option selected value="Performance">Performance</option>
                    <option value="Service Management">Service Management</option>
                    <option value="Governance">Governance</option>
                    <option value="OPCO">OPCO</option>
                </select>
            </div>
            <div v-if="selected == 'Performance'">
                <div class="form-group">
                    <label>JENIS</label>
                    <select class="form-control" name="jenis">
                        <option value="Server" selected>Server</option>
                        <option value="Database">Database</option>
                    </select>
                </div>
            </div>
            <div v-else>
                <div class="form-group">
                    <label>JENIS</label>
                    <select class="form-control" name="jenis">
                        <option value="" selected></option>
                        <option value=""></option>
                    </select>
                </div>
            </div>
          </div>

          <div v-if="category == 'Demand Management'">

            <div class="form-group">
                <label>TYPE</label>
                <select v-model="selected" class="form-control" name="type">
                    <option selected value="MM">MM</option>
                    <option value="FICO">FICO</option>
                    <option value="PM/PS/PP">PM/PS/PP</option>
                    <option value="SD">SD</option>
                </select>
            </div>
            <div v-if="selected == 'MM'">
                <div class="form-group">
                    <label>JENIS</label>
                    <select class="form-control" name="jenis">
                        <option value="Server" selected>Server</option>
                        <option value="Database">Database</option>
                    </select>
                </div>
            </div>
            <div v-else>
                <div class="form-group">
                    <label>JENIS</label>
                    <select class="form-control" name="jenis">
                        <option value="" selected></option>
                        <option value=""></option>
                    </select>
                </div>
            </div>
          </div>

        </div><!-- app vue -->

          <div class="bahan-wrapper">
            <div class="bahan-content row">
              <input type="hidden" name="bahan_count" class="bahan-count">
            <div class="col-md-3">

              <div class="form-group">
                <label>BULAN</label>
                <input type="text" class="form-control bulan" name="bulan[]">
              </div>

            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>ACTUAL</label>
                <input type="text" class="form-control actual" name="actual[]">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>TARGET</label>
                <input type="text" class="form-control target" name="target[]">
              </div>
            </div>

            <div class="col-md-3">
                <label>&nbsp;</label><br>
                <button type="button" class="btn btn-primary btn-sm add-data">TAMBAH</button>
                <button type="button" class="btn btn-success btn-sm remove-data">HAPUS</button>
            </div>



          </div><!--bahan content-->
        </div><!--bahan wrapper-->

        <div class="form-group">
            <label>KETERANGAN</label>
            <textarea name="keterangan" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php //echo $data['deskripsi']; ?></textarea>
        </div>

        <div class="form-group">
              <label>UPLOAD DOCUMENT</label>

                  <div class="dropzone">
                   <div class="dz-message">
                     <h3> Click or Drag Document Here</h3>
                    </div>
                  </div>

          </div><!-- /form-group -->


        <button type="submit" class="btn btn-flat btn-block btn-lg">Submit</button>
    <?php echo form_close(); ?>

</section>
