<script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.27/vue.min.js"></script>
<script src="<?php echo base_url(); ?>assets_theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?php echo base_url(); ?>assets_theme/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets_theme/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets_theme/plugins/dropzone/dropzone.min.js"></script>
<script src="<?php echo base_url(); ?>node_modules/chart.js/dist/Chart.js"></script>
<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>

<script>
    $(document).ready(function(){
    $('#tabel').DataTable();
    });
</script>
<script>
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
        $(".select2").select2();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            toggleActive : true
        });

        new Vue({
            el:'#app'
        });


        /*
        $(document).on("click",".delete_btn",function(){
            var id = $(this).attr("id");
            delete_galeri_foto(id);
        });

        function delete_galeri_foto(id){
            var id_foto_gal=id;
            $.ajax({
              type:"POST",
              url:"<?php echo base_url() ?>kliping/delete_foto_galeri",
              data:{"id":id_foto_gal},
              cache: false,
              success:function(a){
                location.href = location.href;
              },
           });
        }
        */
    });

    Dropzone.autoDiscover = false;

    var foto_upload= new Dropzone(".dropzone",{
    url: "<?php echo base_url('monitoring/proses_upload') ?>",
    maxFilesize: 10,
    method:"post",
    paramName:"userfile",
    acceptedFiles:"application/pdf,.docx,.doc,.ppt,.pptx,.xls,.xlsx",
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
    });

    var sesi=$(".sesi-from_galeri").val();
    var id_kliping=$(".id_kliping").val();
    //nama_koran = $("#id_koran option:selected").text();

    //Event ketika Memulai mengupload
    foto_upload.on("sending",function(a,b,c){
        a.token=Math.random();
        c.append("sesi",sesi);
        c.append("id_kliping",id_kliping);
        //c.append("nama_koran",nama_koran);
        c.append("token",a.token); //Menmpersiapkan token untuk masing masing foto
        console.log(sesi);
    });

    //Event ketika foto dihapus

    foto_upload.on("removedfile",function(a){
        var token=a.token;
        $.ajax({
            type:"post",
            data:{token:token,id_kliping:id_kliping},
            url:"<?php echo base_url('monitoring/remove_foto') ?>",
            cache:false,
            dataType: 'json',
            success: function(){
                console.log(token);
            },
            error: function(){
                console.log("Error");
            }
        });
    });

    $(document).on("click",".delete_btn",function(){
            var id = $(this).attr("id");
            delete_galeri_foto(id);
        });

        function delete_galeri_foto(id){
            var id_foto_gal=id;
            $.ajax({
              type:"POST",
              url:"<?php echo base_url() ?>monitoring/delete_foto_galeri",
              data:{"id":id_foto_gal},
              cache: false,
              success:function(a){
                location.href = location.href;
              },
           });
        }

</script>

<script>
var barChartData = {
          labels: <?php echo $bulan; ?>,
          datasets: [{
              label: "Target",
                  type:'line',
                  data: [100, 100, 100, 100],
                  fill: false,
                  borderColor: '#EC932F',
                  backgroundColor: '#EC932F',
                  pointBorderColor: '#EC932F',
                  pointBackgroundColor: '#EC932F',
                  pointHoverBackgroundColor: '#EC932F',
                  pointHoverBorderColor: '#EC932F'

          }, {
              type: 'bar',
                label: "Actual",
                  fill: false,
                  backgroundColor: '#71B37C',
                  borderColor: '#71B37C',
                  hoverBackgroundColor: '#71B37C',
                  hoverBorderColor: '#71B37C',
                  data: <?php echo $actual; ?>
          } ]
      };

      window.onload = function() {
          var ctx = document.getElementById("barChart").getContext("2d");
          window.myBar = new Chart(ctx, {
              type: 'bar',
              data: barChartData,
              options: {
                  scales: {
                      yAxes: [{
                          ticks: {
                              min:50,
                              max:120,
                              stepSize: 20
                          }
                      }]
                  },
              responsive: true,
              tooltips: {
                mode: 'label'
            },
            elements: {
              line: {
                  fill: false
              }
          },

          }
          });
      };
</script>


<script>
		$(function() {

			if ($('.bahan-content').length > 0) {
				$('.remove-data').attr('disabled','disabled');
			};

			$(document).on('click', '.add-data', function() {
				$this = $(this);
				$clone = $this.closest('.bahan-content').clone();
				var newId = $('.bahan-content').length;
				$clone.find('.bulan').val('');
				$clone.find('.bulan').prop('name', 'bulan[]');
				$clone.find('.actual').val('');
				$clone.find('.actual').prop('name', 'actual[]');
				$clone.find('.target').val('');
				$clone.find('.target').prop('name', 'target[]');
				$clone.appendTo('.bahan-wrapper');
				$('.remove-data').removeAttr('disabled');
				$('.bahan-count').val($('.bahan-content').length);


			});

			$(document).on('click', '.remove-data', function() {
				$this = $(this);
				$clone = $this.closest('.bahan-content').remove();
				if ($('.bahan-content').length == 1) {
					$('.remove-data').attr('disabled','disabled');
				};



			});


		});
</script>
