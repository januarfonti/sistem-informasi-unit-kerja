<section class="content">
    <h1 class="page-header"><i class="fa fa-check-square"></i> PERFORMANCE</h1>
    <?php echo $this->session->flashdata('pesan'); ?>

    <?php if (isset($detail)) { ?>
    <div class="row">
      <div class="col-md-6">
        <!-- BAR CHART -->
        <div class="box box-success">
          <div class="box-body">
            <p><?php echo "NAMA ITEM : ".$detail->nama_item; ?></p>
            <p><?php echo "JENIS : ".$detail->jenis; ?></p>
            <p><?php echo "KETERANGAN : ".$detail->keterangan; ?></p>
         </div>
          <!-- /.box-body -->
        </div>
         <div class="box box-success">
           <div class="box-body">
             <div class="chart">
               <canvas id="barChart" style="max-height:200px !important;"></canvas>
             </div>
          </div>
           <!-- /.box-body -->
         </div>
         <!-- /.box -->
      </div>

      <div class="col-md-6">
        <!-- BAR CHART -->
         <div class="box box-success">
           <div class="box-body">
             <table class="table table-hover">
               <thead>
                 <tr>
                   <th>Bulan</th>
                   <th>Actual</th>
                   <th>Target</th>
                </tr>
               </thead>
               <tbody>
                 <?php if (isset($grafik)){ foreach($grafik as $row){ ?>
                   <tr>
                    <td><?php echo $row['bulan']; ?></td>
                    <td><?php echo $row['actual']; ?></td>
                    <td><?php echo $row['target']; ?></td>
                   </tr>
                 <?php }} ?>

               </tbody>
            </table>
            </div>
           <!-- /.box-body -->
         </div>
         <!-- /.box -->
      </div>
    </div>
    <?php } ?>

    <div class="box box-info">
       <div class="box-body">
         <table id="tabel" class="table table-hover">
           <thead>
             <tr>
               <th>No</th>
               <th>Nama Item</th>
               <th>Jenis Performance</th>
               <th>Keterangan</th>
               <th>Document</th>
               <th>Grafik</th>
             </tr>
           </thead>
           <tbody>
             <?php if (isset($list)) { $no=1; foreach($list as $row) { ?>
             <tr>
               <td><?php echo $no++; ?></td>
               <td><?php echo $row->nama_item; ?></td>
               <td><?php echo $row->jenis; ?></td>
               <td><?php echo $row->keterangan; ?></td>
               <td><a href="<?php echo base_url('monitoring/document/'.$row->id); ?>" class="btn btn-info">DOCUMENT</a></td>
               <td><a href="<?php echo base_url('monitoring/grafik/'.$row->id); ?>" class="btn btn-info">GRAFIK</a></td>
             </tr>
             <?php } } ?>

           </tbody>
           <tfoot>
             <tr>
               <th>No</th>
               <th>Nama Item</th>
               <th>Jenis Performance</th>
               <th>Keterangan</th>
               <th>Document</th>
               <th>Grafik</th>
             </tr>
           </tfoot>
         </table>
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
</section>
