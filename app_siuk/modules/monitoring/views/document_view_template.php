<section class="content">
    <h1 class="page-header"><i class="fa fa-check-square"></i> DOCUMENT MONITORING</h1>
    <?php echo $this->session->flashdata('pesan'); ?>


    <div class="box box-info">
       <div class="box-body">
         <table class="table table-hover">
           <thead>
             <tr>
               <th>No</th>
               <th>Document</th>
             </tr>
           </thead>
           <tbody>
             <?php if (isset($file)) { $no=1; foreach($file as $row) { ?>
             <tr>
               <td><?php echo $no++; ?></td>
               <td><a href="<?php echo base_url('uploads/document/'.$row->name); ?>"><?php echo $row->name; ?></td>
            </tr>
             <?php } } ?>

           </tbody>
           <tfoot>
             <tr>
               <th>No</th>
               <th>Document</th>
             </tr>
           </tfoot>
         </table>
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
</section>
