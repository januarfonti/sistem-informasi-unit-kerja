<section class="content">
    <h1 class="page-header"><i class="fa fa-check-square"></i> <?php echo $judul; ?></h1>
    <?php echo $this->session->flashdata('pesan'); ?>


    <div class="box box-info">
       <div class="box-body">
         <table id="tabel" class="table table-hover">
           <thead>
             <tr>
               <th>No</th>
               <th>Nama Item</th>
               <th>Jenis</th>
               <th>Keterangan</th>
               <th>Document</th>
               <th>Grafik</th>
             </tr>
           </thead>
           <tbody>
             <?php if (isset($list)) { $no=1; foreach($list as $row) { ?>
             <tr>
               <td><?php echo $no++; ?></td>
               <td><?php echo $row->nama_item; ?></td>
               <td><?php echo $row->jenis; ?></td>
               <td><?php echo $row->keterangan; ?></td>
               <td><a href="<?php echo base_url('monitoring/document/'.$row->id); ?>" class="btn btn-info">DOCUMENT</a></td>
               <td><a href="<?php echo base_url('monitoring/grafik/'.$row->id); ?>" class="btn btn-info">GRAFIK</a></td>
             </tr>
             <?php } } ?>

           </tbody>
           <tfoot>
             <tr>
               <th>No</th>
               <th>Nama Item</th>
               <th>Jenis</th>
               <th>Keterangan</th>
               <th>Document</th>
               <th>Grafik</th>
             </tr>
           </tfoot>
         </table>
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
</section>
