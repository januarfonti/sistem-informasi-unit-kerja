<section class="content">
    <h1 class="page-header"><i class="fa fa-check-square"></i> PERFORMANCE</h1>
    <?php echo $this->session->flashdata('pesan'); ?>

    <!-- BAR CHART -->
         <div class="box box-success">
           <div class="box-header with-border">
             <h3 class="box-title">Grafik antara </h3>

             <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
               </button>
               <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
             </div>
           </div>
           <div class="box-body">
             <div class="chart">
               <canvas id="barChart" style="max-height:200px !important;"></canvas>
             </div>
           </div>
           <!-- /.box-body -->
         </div>
         <!-- /.box -->

    <div class="box box-info">
       <div class="box-body">
         <table id="tabel" class="table table-hover">
           <thead>
             <tr>
               <th>No</th>
               <th>Nama Item</th>
               <th>Jenis Performance</th>
               <th>Keterangan</th>
               <th>Document</th>
               <th>Grafik</th>
             </tr>
           </thead>
           <tbody>
             <?php if (isset($list)) { $no=1; foreach($list as $row) { ?>
             <tr>
               <td><?php echo $no++; ?></td>
               <td><?php echo $row->nama_item; ?></td>
               <td><?php echo $row->jenis; ?></td>
               <td><?php echo $row->keterangan; ?></td>
               <td><a href="<?php echo base_url('monitoring/document/'.$row->id); ?>" class="btn btn-info">DOCUMENT</a></td>
               <td><a href="<?php echo base_url('monitoring/grafik/'.$row->id); ?>" class="btn btn-info">GRAFIK</a></td>
             </tr>
             <?php } } ?>

           </tbody>
           <tfoot>
             <tr>
               <th>No</th>
               <th>Nama Audit</th>
               <th>Jenis Audit</th>
               <th>Status Pemenuhan</th>
               <th>PIC</th>
               <th>#</th>
             </tr>
           </tfoot>
         </table>
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
</section>
