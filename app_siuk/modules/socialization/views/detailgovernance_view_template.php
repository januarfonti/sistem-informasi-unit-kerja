<?php if (isset($list)) { ?>
<section class="content">
    <h1 class="page-header"><?php echo $list->governance_name; ?></h1>
      <div class="row">
          <div class="col-md-3 text-center">
              <img src="<?php echo base_url("uploads/catalog/".$list->images); ?>" class="img img-responsive">
              <p style="font-size:20px; color:#fff;">FILE NUMBER : <?php echo $list->file_number; ?></p>
              <p style="font-size:20px; color:#fff;">VALID DATE : <?php echo $list->valid_date; ?></p>
          </div>
          <div class="col-md-9">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_1" data-toggle="tab">DESCRIPTION</a></li>
                      <li><a href="#tab_2" data-toggle="tab">RELATED DOCUMENT</a></li>
                  </ul>
                  <div class="tab-content">
                      <div class="tab-pane active" id="tab_1">
                          <?php echo $list->description; ?>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Document</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php if (isset($doc)) { $no = 1; foreach ($doc as $key) { ?>
                            <tr>
                              <td><?php echo $no++; ?></td>
                              <td><?php echo $key->document_name; ?></td>
                              <td><a href="<?php echo base_url('uploads/document/'.$key->document_name); ?>" class="btn btn-info btn-sm">View</a></td>
                            </tr>
                            <?php }$no++;} ?>
                          </tbody>
                        </table>
                      </div>
                      <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
              </div>
              <!-- nav-tabs-custom -->
          </div>
      </div>
</section>
  <?php } ?>
