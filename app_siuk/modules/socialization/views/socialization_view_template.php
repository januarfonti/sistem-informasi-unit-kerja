    <section class="content">
        <h1 class="page-header">CATALOG SERVICE</h1>
        <div class="row">
            <div class="col-md-2">
                <a href="<?php echo base_url('socialization/infrastructure'); ?>">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>INFRASTRUCTURE</h3>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_theme/img/infra.jpg" alt="...">
                </div>
                </a>
            </div>

            <div class="col-md-2">
                <a href="<?php echo base_url('socialization/application'); ?>">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3>APPLICATION</h3>
                        </div>
                        <img src="<?php echo base_url(); ?>assets_theme/img/app.jpg" alt="...">
                    </div>
                </a>
            </div>
        </div>

        <h1 class="page-header">GOVERNANCE</h1>
        <div class="row">
            <div class="col-md-2">
                <a href="<?php echo base_url('socialization/policy'); ?>">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3>POLICY</h3>
                        </div>
                        <img src="<?php echo base_url(); ?>assets_theme/img/policy.jpg" alt="...">
                    </div>
                </a>
            </div>

            <div class="col-md-2">
                <a href="<?php echo base_url('socialization/procedure'); ?>">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3>PROCEDURE</h3>
                        </div>
                        <img src="<?php echo base_url(); ?>assets_theme/img/procedure.jpg" alt="...">
                    </div>
                </a>
            </div>

            <div class="col-md-2">
                <a href="<?php echo base_url('socialization/workinstruction'); ?>">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3>WORK INSTRUCTION</h3>
                        </div>
                        <img src="<?php echo base_url(); ?>assets_theme/img/work.jpg" alt="...">
                    </div>
                </a>
            </div>

            <div class="col-md-2">
                <a href="<?php echo base_url('socialization/guidance'); ?>">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3>GUIDANCE</h3>
                        </div>
                        <img src="<?php echo base_url(); ?>assets_theme/img/guide.jpg" alt="...">
                    </div>
                </a>
            </div>
        </div>
    </section>
