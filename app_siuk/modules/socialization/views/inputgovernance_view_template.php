<section class="content">
    <h1 class="page-header">INPUT GOVERNANCE</h1>
    <?php echo $this->session->flashdata('pesan'); ?>
    <?php echo form_open_multipart('socialization/savegovernance'); ?>
    <input type='hidden' name='sesi' class='sesi-from_galeri' value='<?php echo rand(0,100).rand(10,500).date('dym') ?>' >
    <input type='hidden' class='id_kliping' name="id_kliping" value='0' >
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>GOVERNANCE NAME</label>
                    <input type="text" class="form-control" name="governance_name">
                </div>

                    <div class="form-group">
                        <label>CATEGORY</label>
                        <select v-model="selected" class="form-control" name="category">
                            <option selected value="Policy">Policy</option>
                            <option value="Procedure">Procedure</option>
                            <option value="Work Instruction">Work Instruction</option>
                            <option value="Guidance">Guidance</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>FILE NUMBER</label>
                        <input type="text" class="form-control" name="file_number">
                    </div>


            </div>
            <div class="col-md-6">

              <div class="form-group">
                  <label>VALID DATE</label>
                  <input type="text" class="form-control datepicker" name="valid_date">
              </div>

              <div class="form-group">
                    <label>IMAGE UPLOAD</label>
                    <input name="userfile" type="file">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>SERVICE DESCRIPTION</label>
            <textarea name="description" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
        </div>

        <div class="form-group">
              <label>RELATED DOCUMENT</label>

                  <div class="dropzone">
                   <div class="dz-message">
                     <h3> Click or Drag Document Here</h3>
                    </div>
                  </div>

          </div><!-- /form-group -->


        <button type="submit" class="btn btn-flat btn-block btn-lg">Submit</button>
    <?php echo form_close(); ?>

</section>
