<section class="content">
    <h1 class="page-header">INFRASTRUCTURE</h1>
    <div class="row">
        <div class="col-md-2">
            <a href="<?php echo base_url('socialization/network'); ?>">
            <div class="thumbnail">
                <img src="http://placehold.it/500/3c8dbc/ffffff?text=NETWORK" alt="...">
            </div>
            </a>
        </div>

        <div class="col-md-2">
            <a href="<?php echo base_url('socialization/device'); ?>">
                <div class="thumbnail">
                    <img src="http://placehold.it/500/3c8dbc/ffffff?text=DEVICE" alt="...">
                </div>
            </a>
        </div>

        <div class="col-md-2">
            <a href="<?php echo base_url('socialization/meeting'); ?>">
                <div class="thumbnail">
                    <img src="http://placehold.it/500/3c8dbc/ffffff?text=MEETING" alt="...">
                </div>
            </a>
        </div>
    </div>

</section>
