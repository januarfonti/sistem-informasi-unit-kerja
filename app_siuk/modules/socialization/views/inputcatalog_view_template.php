<section class="content">
    <h1 class="page-header">INPUT CATALOG SERVICE</h1>
    <?php echo $this->session->flashdata('pesan'); ?>
    <?php echo form_open_multipart('socialization/savecatalog'); ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>CATALOG NAME</label>
                    <input type="text" class="form-control" name="catalog_name">
                </div>
                <div id='app'>
                    <div class="form-group">
                        <label>CATEGORY</label>
                        <select v-model="selected" class="form-control" name="category">
                            <option selected value="Infrastructure">Infrastructure</option>
                            <option value="Application">Application</option>
                        </select>
                    </div>
                    <div v-if="selected == 'Infrastructure'">
                        <div class="form-group">
                            <label>ITEM</label>
                            <select class="form-control" name="item">
                                <option value="Network" selected>Network</option>
                                <option value="Device">Device</option>
                                <option value="Meeting">Meeting</option>
                            </select>
                        </div>
                    </div>
                    <div v-else>
                        <div class="form-group">
                            <label>ITEM</label>
                            <select class="form-control" name="item">
                                <option value="ERP" selected>ERP</option>
                                <option value="Non ERP">Non ERP</option>
                            </select>
                        </div>
                    </div>
                </div><!-- app vue -->


            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>DELIVERED TO</label>
                    <select class="form-control select2" name="delivered[]" multiple="multiple" data-placeholder="Select an item" style="width: 100%;">
                        <option value="Karyawan Semen Indonesia">Karyawan Semen Indonesia</option>
                        <option value="Karyawan Outsourcing">karyawan Outsourcing</option>
                        <option value="Tamu Semen Indonesia">Tamu Semen Indonesia</option>

                    </select>
                </div>
                <div class="form-group">
                    <label>HOT TO GET</label>
                    <select name="howtoget[]"class="form-control select2" multiple="multiple" data-placeholder="Select an item" style="width: 100%;">
                        <option value="Call 51">Call 51</option>
                        <option value="Email servicedesk@semenindonesia.com">Email servicedesk@semenindonesia.com</option>
                        <option value="Chat BBM 551BN#">Chat BBM 551BN#</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>IMAGE UPLOAD</label>
                    <input name="userfile" type="file">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>SERVICE DESCRIPTION</label>
            <textarea name="description" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
        </div>


        <button type="submit" class="btn btn-flat btn-block btn-lg">Submit</button>
    <?php echo form_close(); ?>

</section>
