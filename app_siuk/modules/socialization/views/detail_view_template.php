<?php if (isset($list)) { ?>
<section class="content">
    <h1 class="page-header"><?php echo $list->catalog_name; ?></h1>
      <div class="row">
          <div class="col-md-3">
              <img src="<?php echo base_url("uploads/catalog/".$list->images); ?>" class="img img-responsive">
          </div>
          <div class="col-md-9">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_1" data-toggle="tab">SERVICE DESCRIPTION</a></li>
                      <li><a href="#tab_2" data-toggle="tab">DELIVERED TO</a></li>
                      <li><a href="#tab_3" data-toggle="tab">HOW TO GET ?</a></li>

                  </ul>
                  <div class="tab-content">
                      <div class="tab-pane active" id="tab_1">
                          <?php echo $list->description; ?>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2">
                        <?php $arr_delivered = explode(",",$list->delivered);
                        echo "<ol>";
                        foreach ($arr_delivered as $str) {
                            echo "<li>".$str."</li>";
                        }
                        echo "</ol>";
                        ?>

                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_3">
                        <?php $arr_howtoget = explode(",",$list->howtoget);
                        echo "<ul>";
                        foreach ($arr_howtoget as $str) {
                            echo "<li>".$str."</li>";
                        }
                        echo "</ul>";
                        ?>
                      </div>
                      <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
              </div>
              <!-- nav-tabs-custom -->
          </div>
      </div>
</section>
  <?php } ?>
