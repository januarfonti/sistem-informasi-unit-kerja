<section class="content">
    <h1 class="page-header">MEETING</h1>
    <div class="row">
      <?php if (isset($list)) { foreach ($list as $row){ ?>
        <div class="col-md-2 text-center">
            <a href="<?php echo base_url('socialization/detail/'.$row->id); ?>">
            <div class="thumbnail">
                <img src="<?php echo base_url('uploads/catalog/'.$row->images); ?>" alt="...">
            </div>
                <div class="text">
                    <h4><?php echo $row->catalog_name; ?></h4>
                </div>
            </a>
        </div>
        <?php }} else { echo "Data Kosong"; } ?>


    </div>

</section>
