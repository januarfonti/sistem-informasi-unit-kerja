<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Socialization extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Socialization_model','socialization');
	}

	public function index()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
    $this->load_view('socialization_view_template');
	}

	public function infrastructure()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
    $this->load_view('infrastructure_view_template');
	}

	public function network()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->socialization->get_network();
    $this->load_view('network_view_template',$data);
	}

	public function device()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->socialization->get_device();
    $this->load_view('device_view_template',$data);
	}

	public function meeting()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->socialization->get_meeting();
    $this->load_view('meeting_view_template',$data);
	}

	public function application()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
    $this->load_view('application_view_template');
	}

	public function erp()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->socialization->get_erp();
    $this->load_view('erp_view_template',$data);
	}

	public function nonerp()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->socialization->get_nonerp();
    $this->load_view('nonerp_view_template',$data);
	}

	public function detail($id)
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
		$data['list'] = $this->socialization->get_by_id($id);
    $this->load_view('detail_view_template',$data);
	}

	public function inputcatalog()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
    $this->load_view('inputcatalog_view_template');
	}

	public function savecatalog()
	{
			$config['upload_path']      = FCPATH.'uploads/catalog/';
      $config['allowed_types'] 		= 'gif|jpg|png';

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('userfile'))
      {
      	//$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Catalog Gagal Ditambahkan ! Pastikan anda telah memilih file gambar dengan benar (jpeg / jpg / png).</div>");
				redirect('socialization/inputcatalog');
      }
      else
      {
      	$file              		= $this->upload->data();
				$data['catalog_name'] = $this->input->post('catalog_name');
				$data['category']     = $this->input->post('category');
				$data['item']         = $this->input->post('item');
				$delivered            = $this->input->post('delivered');
				$data['delivered']    = implode(",",$delivered);
				$howtoget             = $this->input->post('howtoget');
				$data['howtoget']     = implode(",",$howtoget);
				$data['images']       = $file['file_name'];
				$data['description']  = $this->input->post('description');
				$this->socialization->save($data);
				$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Ditambahkan ! </div>");
				redirect('socialization/inputcatalog');
      }

	}

	public function inputgovernance()
	{
		$this->load->helper('url');
    $this->set_template('backend');
    $p_css = $this->load->view('socialization_view_css',array(),true);
    $this->add_template_html_header($p_css);
    $p_js = $this->load->view('socialization_view_js',array(),true);
    $this->add_template_html_footer($p_js);
    $this->load_view('inputgovernance_view_template');
	}

	public function proses_upload(){

        $config['upload_path']   = FCPATH.'/uploads/document/';
        //$new_name = $this->input->post('nama_koran');
				//$config['file_name'] = $new_name;
				$config['allowed_types'] = 'pdf|docx|doc';
        $this->load->library('upload',$config);

        if($this->upload->do_upload('userfile')){
					$token = $this->input->post('token');
					$nama  = $this->upload->data('file_name');
					$sesi  = $this->input->post('sesi');
					$this->db->insert('siuk_governance_temp',array('document_name'=>$nama,'token'=>$token,'sesi_form'=>$sesi));
	      }
    }

		//Untuk menghapus foto
    public function remove_foto(){

        //Ambil token foto
        $token=$this->input->post('token');
        $id_kliping=$this->input->post('id_kliping');

				$kliping=$this->db->get_where('siuk_governance_temp',array('token'=>$token));
				if($kliping->num_rows()>0){
						$hasil=$kliping->row();
						$document_name=$hasil->document_name;
						if(file_exists($file=FCPATH.'/uploads/document/'.$document_name)){
								unlink($file);
						}
						$this->db->delete('siuk_governance_temp',array('token'=>$token));
				}
				echo "{}";
    }

		public function savegovernance()
		{
			$config['upload_path']      = FCPATH.'uploads/catalog/';
      $config['allowed_types'] 		= 'gif|jpg|png';

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('userfile'))
      {
      	//$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Catalog Gagal Ditambahkan ! Pastikan anda telah memilih file gambar dengan benar (jpeg / jpg / png).</div>");
				redirect('socialization/inputcatalog');
      }
      else
      {
				$file            = $this->upload->data();
				$sesi						 = $this->input->post('sesi');
				$governance_name = $this->input->post('governance_name');
				$category        = $this->input->post('category');
				$file_number     = $this->input->post('file_number');
				$valid_date      = $this->input->post('valid_date');
				$images          = $file['file_name'];
				$description     = $this->input->post('description');


				$query=$this->db->query("INSERT INTO siuk_governance (governance_name,category,file_number,valid_date,images,description) VALUES ('$governance_name','$category','$file_number','$valid_date','$images','$description')");
					$new_id=$this->db->insert_id();

					$foto=$this->db->query("SELECT * FROM siuk_governance_temp WHERE sesi_form='$sesi' ORDER BY id");
					if($foto->num_rows()>0)
					{
						foreach($foto->result_array() AS $data)
						{
							$this->db->query("INSERT INTO siuk_governance_document (id_governance,document_name) VALUES ('$new_id','$data[document_name]')");
						}
						$this->db->query("DELETE FROM siuk_governance_temp WHERE sesi_form='$sesi'");
					}
					$this->session->set_flashdata("pesan","<div class='alert alert-success alert-dismissible fade in' role=alert> <button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>Data Berhasil Disimpan ! </div>");
					redirect('socialization/inputgovernance');
      }
		}

		public function policy()
		{
			$this->load->helper('url');
	    $this->set_template('backend');
	    $p_css = $this->load->view('socialization_view_css',array(),true);
	    $this->add_template_html_header($p_css);
	    $p_js = $this->load->view('socialization_view_js',array(),true);
	    $this->add_template_html_footer($p_js);
			$data['list'] = $this->socialization->get_policy();
	    $this->load_view('policy_view_template',$data);
		}

		public function procedure()
		{
			$this->load->helper('url');
	    $this->set_template('backend');
	    $p_css = $this->load->view('socialization_view_css',array(),true);
	    $this->add_template_html_header($p_css);
	    $p_js = $this->load->view('socialization_view_js',array(),true);
	    $this->add_template_html_footer($p_js);
			$data['list'] = $this->socialization->get_procedure();
	    $this->load_view('procedure_view_template',$data);
		}

		public function workinstruction()
		{
			$this->load->helper('url');
	    $this->set_template('backend');
	    $p_css = $this->load->view('socialization_view_css',array(),true);
	    $this->add_template_html_header($p_css);
	    $p_js = $this->load->view('socialization_view_js',array(),true);
	    $this->add_template_html_footer($p_js);
			$data['list'] = $this->socialization->get_work();
	    $this->load_view('work_view_template',$data);
		}

		public function guidance()
		{
			$this->load->helper('url');
	    $this->set_template('backend');
	    $p_css = $this->load->view('socialization_view_css',array(),true);
	    $this->add_template_html_header($p_css);
	    $p_js = $this->load->view('socialization_view_js',array(),true);
	    $this->add_template_html_footer($p_js);
			$data['list'] = $this->socialization->get_guidance();
	    $this->load_view('guidance_view_template',$data);
		}

		public function detailgovernance($id)
		{
			$this->load->helper('url');
	    $this->set_template('backend');
	    $p_css = $this->load->view('socialization_view_css',array(),true);
	    $this->add_template_html_header($p_css);
	    $p_js = $this->load->view('socialization_view_js',array(),true);
	    $this->add_template_html_footer($p_js);
			$data['list'] = $this->socialization->get_by_id_governance($id);
			$data['doc'] = $this->socialization->get_document($id);
	    $this->load_view('detailgovernance_view_template',$data);
		}



}
