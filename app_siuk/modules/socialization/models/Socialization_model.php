<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Socialization_model extends CI_Model {

	var $table = 'siuk_catalog';
	//var $column = array('firstname','lastname','gender','address','dob'); //set column field database for order and search
	var $order = array('id' => 'desc'); // default order

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_network()
	{
		$this->db->select('id,catalog_name,images');
		$this->db->from($this->table);
		$this->db->where('item','Network');
		$query = $this->db->get();
		return $query->result();
	}

	function get_device()
	{
		$this->db->select('id,catalog_name,images');
		$this->db->from($this->table);
		$this->db->where('item','Device');
		$query = $this->db->get();
		return $query->result();
	}

	function get_meeting()
	{
		$this->db->select('id,catalog_name,images');
		$this->db->from($this->table);
		$this->db->where('item','Meeting');
		$query = $this->db->get();
		return $query->result();
	}

	function get_erp()
	{
		$this->db->select('id,catalog_name,images');
		$this->db->from($this->table);
		$this->db->where('item','ERP');
		$query = $this->db->get();
		return $query->result();
	}

	function get_nonerp()
	{
		$this->db->select('id,catalog_name,images');
		$this->db->from($this->table);
		$this->db->where('item','Non ERP');
		$query = $this->db->get();
		return $query->result();
	}

	function get_policy()
	{
		$this->db->select('id,governance_name,images');
		$this->db->from('siuk_governance');
		$this->db->where('category','Policy');
		$query = $this->db->get();
		return $query->result();
	}

	function get_procedure()
	{
		$this->db->select('id,governance_name,images');
		$this->db->from('siuk_governance');
		$this->db->where('category','Procedure');
		$query = $this->db->get();
		return $query->result();
	}

	function get_work()
	{
		$this->db->select('id,governance_name,images');
		$this->db->from('siuk_governance');
		$this->db->where('category','Work Instruction');
		$query = $this->db->get();
		return $query->result();
	}

	function get_guidance()
	{
		$this->db->select('id,governance_name,images');
		$this->db->from('siuk_governance');
		$this->db->where('category','Guidance');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_id_governance($id)
	{
		$this->db->from('siuk_governance');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_document($id)
	{
		$this->db->from('siuk_governance_document');
		$this->db->where('id_governance',$id);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}


}
