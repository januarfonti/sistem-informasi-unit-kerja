<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIUK</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_theme/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_theme/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_theme/dist/css/skins/skin-blue.css">
    <link href="https://fonts.googleapis.com/css?family=Pathway+Gothic+One" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_theme/dist/css/front.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <div class="container">
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                        <li><a href="#" id="carousel-selector-0">ICT SOCIALIZATION</a></li>
                        <li><a href="#" id="carousel-selector-1">ISSUE</a></li>
                        <li><a href="#" id="carousel-selector-2">DOCUMENT</a></li>
                        <li><a href="#" id="carousel-selector-3">AUDITOR</a> </li>
                        <li><a href="#" id="carousel-selector-4">MONITORING</a></li>
                        <li><a href="#" id="carousel-selector-5">OFFICE</a> </li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>
    <!-- Full Width Column -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper text-center">
        <!-- Content Header (Page header) -->
        <img style="width:200px; height:auto; padding-top:50px;" src="<?php echo base_url('assets_theme/img/logo.png'); ?>">
        <!-- Slider -->
        <div class="row">
            <div class="col-xs-12 start-slider" id="slider">
                <!-- Top part of the slider -->
                <div class="row">
                    <div class="col-md-12" id="carousel-bounding-box">
                        <div class="carousel slide carousel-fade" id="myCarousel">
                            <!-- Carousel items -->
                            <div class="carousel-inner text-center">
                                <div class="active item" data-slide-number="0">
                                    <p>ICT SOCIALIZATION</p>
                                    <a href="<?php echo base_url('socialization'); ?>" class="btn btn-flat btn-lg">ENTER</a>
                                </div>
                                <div class="item" data-slide-number="1">
                                    <p>ISSUE</p>
                                    <a href="<?php echo base_url('issue'); ?>" class="btn btn-flat btn-lg">ENTER</a>
                                </div>
                                <div class="item" data-slide-number="2">
                                    <p>DOCUMENT</p>
                                    <a href="<?php echo base_url('document'); ?>" class="btn btn-flat btn-lg">ENTER</a>
                                </div>
                                <div class="item" data-slide-number="3">
                                    <p>AUDITOR</p>
                                    <a href="<?php echo base_url('auditor'); ?>" class="btn btn-flat btn-lg">ENTER</a>
                                </div>
                                <div class="item" data-slide-number="4">
                                    <p>MONITORING</p>
                                    <a href="<?php echo base_url('monitoring'); ?>" class="btn btn-flat btn-lg">ENTER</a>
                                </div>
                                <div class="item" data-slide-number="5">
                                    <p>OFFICE</p>
                                    <a href="<?php echo base_url('office'); ?>" class="btn btn-flat btn-lg">ENTER</a>
                                </div>

                            </div><!-- Carousel nav -->
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-4" id="carousel-text"></div>


                </div>
            </div>
        </div><!--/Slider-->

    </div>
    <!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>assets_theme/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets_theme/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets_theme/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets_theme/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets_theme/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script>
    jQuery(document).ready(function($) {

        $('#myCarousel').carousel({
            interval: 5000
        });

        $('#carousel-text').html($('#slide-content-0').html());

        //Handles the carousel thumbnails
        $('[id^=carousel-selector-]').click( function(){
            var id = this.id.substr(this.id.lastIndexOf("-") + 1);
            var id = parseInt(id);
            $('#myCarousel').carousel(id);
        });


        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid.bs.carousel', function (e) {
            var id = $('.item.active').data('slide-number');
            $('#carousel-text').html($('#slide-content-'+id).html());
        });
    });
</script>
</body>
</html>
