<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>SI</b> Unit Kerja</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>SI</b> Unit Kerja</span>
    </a>
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <!--<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>-->
        <div class="container">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                    <li><a href="<?php echo base_url('socialization'); ?>">ICT SOCIALIZATION</a></li>
                    <li><a href="<?php echo base_url('issue'); ?>">ISSUE</a></li>
                    <li><a href="<?php echo base_url('document'); ?>">DOCUMENT</a></li>
                    <li><a href="<?php echo base_url('auditor'); ?>">AUDITOR</a> </li>
                    <li><a href="<?php echo base_url('monitoring'); ?>">MONITORING</a></li>
                    <li><a href="<?php echo base_url('office'); ?>">OFFICE</a> </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>
