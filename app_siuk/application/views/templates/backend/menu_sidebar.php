<aside class="main-sidebar">
    <?php $menu = $this->uri->segment(1); if ($menu == 'socialization') { ?>
        <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li>&nbsp;</li>
          <li class="header">MAIN MENU</li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i> <span>CATALOG SERVICE</span>
                    <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('socialization/infrastructure'); ?>"><i class="fa fa-code-fork"></i> INSFRASTRUCTURE</a></li>
                    <li><a href="<?php echo base_url('socialization/application'); ?>"><i class="fa fa-code"></i> APPLICATION</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-diamond"></i> <span>GOVERNANCE</span>
                    <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('socialization/policy'); ?>"><i class="fa fa-code-fork"></i> POLICY</a></li>
                    <li><a href="<?php echo base_url('socialization/procedure'); ?>"><i class="fa fa-code"></i> PROCEDURE</a></li>
                    <li><a href="<?php echo base_url('socialization/workinstruction'); ?>"><i class="fa fa-code-fork"></i> WORK INSTRUCTION</a></li>
                    <li><a href="<?php echo base_url('socialization/guidance'); ?>"><i class="fa fa-code"></i> GUIDANCE</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-plus"></i> <span>INPUT</span>
                    <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('socialization/inputcatalog'); ?>"><i class="fa fa-list"></i> CATALOG SERVICE</a></li>
                    <li><a href="<?php echo base_url('socialization/inputgovernance'); ?>"><i class="fa fa-diamond"></i> GOVERNANCE</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <?php } elseif ($menu == 'issue') { ?>
      <section class="sidebar">

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li>&nbsp;</li>
          <li><a href="<?php echo base_url('issue'); ?>"><i class="fa fa-check-square-o"></i> <span>ISSUE LAYANAN</span></a></li>
          <li><a href="<?php echo base_url('issue/input'); ?>"><i class="fa fa-plus"></i> <span>INPUT ISSUE</span></a></li>
      </ul>
  </section>
  <?php } elseif ($menu == 'document') { ?>
    <section class="sidebar">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li>&nbsp;</li>
        <li><a href="<?php echo base_url('document'); ?>"><i class="fa fa-check-square-o"></i> <span>LIST DOCUMENT</span></a></li>
        <li><a href="<?php echo base_url('document/input'); ?>"><i class="fa fa-plus"></i> <span>INPUT DOCUMENT</span></a></li>
    </ul>
</section>
<?php } elseif ($menu == 'auditor') { ?>
  <section class="sidebar">

  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li>&nbsp;</li>
      <li><a href="<?php echo base_url('auditor'); ?>"><i class="fa fa-check-square-o"></i> <span>LIST AUDIT</span></a></li>
      <li><a href="<?php echo base_url('auditor/input'); ?>"><i class="fa fa-plus"></i> <span>INPUT AUDIT</span></a></li>
  </ul>
</section>
<?php } elseif ($menu == 'monitoring') { ?>
  <section class="sidebar">

  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li>&nbsp;</li>
    <li class="header">LAPORAN</li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-database"></i> <span>Performance & Governance</span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo base_url('monitoring/performance'); ?>"><i class="fa fa-toggle-on"></i> PERFORMANCE</a></li>
            <li><a href="<?php echo base_url('monitoring/servicemanagement'); ?>"><i class="fa fa-toggle-on"></i> SERVICE</a></li>
            <li><a href="<?php echo base_url('monitoring/governance'); ?>"><i class="fa fa-toggle-on"></i> GOVERNANCE</a></li>
            <li><a href="<?php echo base_url('monitoring/opco'); ?>"><i class="fa fa-toggle-on"></i> OPCO</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-list"></i> <span>Demand Management</span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo base_url('monitoring/mm'); ?>"><i class="fa fa-toggle-on"></i> MM</a></li>
            <li><a href="<?php echo base_url('monitoring/fico'); ?>"><i class="fa fa-toggle-on"></i> FICO</a></li>
            <li><a href="<?php echo base_url('monitoring/pm'); ?>"><i class="fa fa-toggle-on"></i> PM/PS/PP</a></li>
            <li><a href="<?php echo base_url('monitoring/sd'); ?>"><i class="fa fa-toggle-on"></i> SD</a></li>
        </ul>
    </li>
    <li class="header">INPUT</li>
    <li><a href="<?php echo base_url('monitoring/input'); ?>"><i class="fa fa-plus"></i> <span>Input Monitoring</span></a></li>
  </ul>
</section>
<?php } elseif ($menu == 'office') { ?>
  <section class="sidebar">

  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li>&nbsp;</li>
    <li class="header">LIST</li>
      <li><a href="<?php echo base_url('office/notulen'); ?>"><i class="fa fa-book"></i> <span>NOTULEN</span></a></li>
      <li><a href="<?php echo base_url('office/korin'); ?>"><i class="fa fa-sticky-note"></i> <span>KORIN</span></a></li>
      <li class="header">INPUT</li>
      <li><a href="<?php echo base_url('office/inputnotulen'); ?>"><i class="fa fa-plus"></i> <span>NOTULEN</span></a></li>
      <li><a href="<?php echo base_url('office/inputkorin'); ?>"><i class="fa fa-plus"></i> <span>KORIN</span></a></li>
  </ul>
</section>
<?php } ?>
</aside>
