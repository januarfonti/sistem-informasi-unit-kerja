
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>assets_theme/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets_theme/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets_theme/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets_theme/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets_theme/dist/js/app.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jquery.form-validator.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script>
    $( document ).ready(function() {
        $("[rel='tooltip']").tooltip();

        $('.thumbnail').hover(
                function(){
                    $(this).find('.caption').slideDown(250); //.fadeIn(250)
                },
                function(){
                    $(this).find('.caption').slideUp(250); //.fadeOut(205)
                }
        );
    });
</script>
