-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 13, 2016 at 10:43 PM
-- Server version: 5.6.27-75.0
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `januarfo_siuk`
--

-- --------------------------------------------------------

--
-- Table structure for table `siuk_auditor`
--

CREATE TABLE IF NOT EXISTS `siuk_auditor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_auditor` varchar(255) NOT NULL DEFAULT '',
  `jenis_auditor` varchar(100) NOT NULL DEFAULT '',
  `status_pemenuhan` varchar(50) NOT NULL DEFAULT '',
  `target_pemenuhan` date NOT NULL,
  `lokasi` varchar(500) NOT NULL DEFAULT '',
  `pic` varchar(500) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_auditor_file`
--

CREATE TABLE IF NOT EXISTS `siuk_auditor_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_auditor` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `token` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_auditor_temp`
--

CREATE TABLE IF NOT EXISTS `siuk_auditor_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(50) NOT NULL,
  `token` varchar(500) NOT NULL,
  `sesi_form` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_catalog`
--

CREATE TABLE IF NOT EXISTS `siuk_catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catalog_name` varchar(255) NOT NULL,
  `category` varchar(100) NOT NULL,
  `item` varchar(100) NOT NULL,
  `delivered` varchar(255) NOT NULL,
  `howtoget` varchar(255) NOT NULL,
  `images` varchar(300) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_document`
--

CREATE TABLE IF NOT EXISTS `siuk_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_document` varchar(255) NOT NULL,
  `jenis_document` varchar(100) NOT NULL,
  `status_document` varchar(50) NOT NULL,
  `masa_berlaku` date NOT NULL,
  `lokasi_document` varchar(500) NOT NULL,
  `pic` varchar(500) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_document_file`
--

CREATE TABLE IF NOT EXISTS `siuk_document_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_document` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `token` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_document_temp`
--

CREATE TABLE IF NOT EXISTS `siuk_document_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(50) NOT NULL,
  `token` varchar(500) NOT NULL,
  `sesi_form` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_governance`
--

CREATE TABLE IF NOT EXISTS `siuk_governance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `governance_name` varchar(255) NOT NULL,
  `category` varchar(100) NOT NULL,
  `file_number` varchar(50) NOT NULL,
  `valid_date` date NOT NULL,
  `images` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_governance_document`
--

CREATE TABLE IF NOT EXISTS `siuk_governance_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_governance` int(11) NOT NULL,
  `document_name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_governance_temp`
--

CREATE TABLE IF NOT EXISTS `siuk_governance_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(50) NOT NULL,
  `token` varchar(500) NOT NULL,
  `sesi_form` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_issue`
--

CREATE TABLE IF NOT EXISTS `siuk_issue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue` varchar(500) NOT NULL,
  `category_issue` varchar(500) NOT NULL DEFAULT '',
  `lokasi` varchar(255) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(255) NOT NULL DEFAULT '',
  `deskripsi_issue` text NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_korin`
--

CREATE TABLE IF NOT EXISTS `siuk_korin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_korin` varchar(500) NOT NULL DEFAULT '',
  `no_korin` varchar(500) NOT NULL DEFAULT '',
  `kepada` varchar(500) NOT NULL DEFAULT '',
  `dari` varchar(500) NOT NULL DEFAULT '',
  `perihal` varchar(500) NOT NULL DEFAULT '',
  `isi_korin` text NOT NULL,
  `tanggal` date NOT NULL,
  `lokasi` varchar(255) NOT NULL DEFAULT '',
  `pejabat` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `siuk_korin`
--

INSERT INTO `siuk_korin` (`id`, `nama_korin`, `no_korin`, `kepada`, `dari`, `perihal`, `isi_korin`, `tanggal`, `lokasi`, `pejabat`) VALUES
(1, 'Korespondensi Intern', '9898/KU.98.89/98989/89.8989', 'Direktur Keuangan', 'Departement of Strategic ICT', 'Permohonan Persetujuan Pengadaan Jasa Annual Support Fortinet Security PT. Semen Indonesia (Persero) Tbk.', '<p>Dengan hormat,</p><p>Sehubungan dengan kebutuhan terhadap pentingnya firewall sebagai pengamanan jaringan di PT. Semen Indonesia, kami mohon persetujuan&nbsp;<b>Pengadaan Jasa Annual Support Fortinet Security PT. Semen Indonesia (Persero) Tbk</b>, sesuai dengan lampiran TOR dan ketentuan yang berlaku di Bagian Pengadaan Jasa.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pulvinar vel augue venenatis posuere. Morbi scelerisque neque a justo tristique, vitae tempor metus sagittis. In eu porta urna, quis cursus purus. Donec nec posuere erat, quis ultricies mi. Integer cursus, nunc at porttitor eleifend, nulla velit porttitor diam, sit amet condimentum urna dolor nec massa.<br></p>', '2016-10-12', 'Gresik', 'Januar Fonti');

-- --------------------------------------------------------

--
-- Table structure for table `siuk_monitoring`
--

CREATE TABLE IF NOT EXISTS `siuk_monitoring` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_item` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `keterangan` text NOT NULL,
  `jenis` varchar(255) NOT NULL DEFAULT '',
  `kategori` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `siuk_monitoring`
--

INSERT INTO `siuk_monitoring` (`id`, `nama_item`, `type`, `keterangan`, `jenis`, `kategori`) VALUES
(7, 'Peromance & Governance', 'Performance', '<p>SLA tercapai?</p>', 'Server', 'Performance & Governance');

-- --------------------------------------------------------

--
-- Table structure for table `siuk_monitoring_data`
--

CREATE TABLE IF NOT EXISTS `siuk_monitoring_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_monitoring` int(11) NOT NULL,
  `bulan` varchar(255) NOT NULL DEFAULT '',
  `actual` int(50) NOT NULL,
  `target` int(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `siuk_monitoring_data`
--

INSERT INTO `siuk_monitoring_data` (`id`, `id_monitoring`, `bulan`, `actual`, `target`) VALUES
(14, 7, 'April', 99, 100),
(13, 7, 'Maret', 97, 100),
(12, 7, 'Februari', 98, 100),
(11, 7, 'Januari', 99, 100);

-- --------------------------------------------------------

--
-- Table structure for table `siuk_monitoring_file`
--

CREATE TABLE IF NOT EXISTS `siuk_monitoring_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_monitoring` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `token` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_monitoring_temp`
--

CREATE TABLE IF NOT EXISTS `siuk_monitoring_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(50) NOT NULL,
  `token` varchar(500) NOT NULL,
  `sesi_form` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `siuk_notulen`
--

CREATE TABLE IF NOT EXISTS `siuk_notulen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_rapat` varchar(255) NOT NULL DEFAULT '',
  `waktu` date NOT NULL,
  `jam` varchar(50) NOT NULL DEFAULT '',
  `lokasi` varchar(500) NOT NULL DEFAULT '',
  `agenda` varchar(500) NOT NULL DEFAULT '',
  `chair_person` varchar(500) NOT NULL DEFAULT '',
  `submitted` varchar(500) NOT NULL DEFAULT '',
  `in_attendance` text NOT NULL,
  `kesimpulan` text NOT NULL,
  `ringkasan` text NOT NULL,
  `action_plan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `siuk_notulen`
--

INSERT INTO `siuk_notulen` (`id`, `nama_rapat`, `waktu`, `jam`, `lokasi`, `agenda`, `chair_person`, `submitted`, `in_attendance`, `kesimpulan`, `ringkasan`, `action_plan`) VALUES
(2, 'Nama Rapat Ubah', '2016-10-27', '', 'Gresik', 'Isi Agenda', 'Fonti', 'Submit', '<ol><li>Asri Wahjusukrisno</li><li>Icuk Hertanto</li><li>Rudy Akay</li><li>Indra Noviandi</li><li>Teguh Hariono</li><li>Zaenul Arifin</li><li>Harmadi</li><li>Bambang Trimono</li><li>Denni Rianto</li><li>Agus Purnomo</li><li>Robbi R. Putra</li><li>Horas Siburian</li><li>Redi Garjito</li></ol>', '<ol><li>Ini adalah kesimpulan satu</li><li>Ini adalah kesimpulan dua</li><li>Ini adalah kesimpulan tiga</li></ol>', '<p>Skenario Penggunaan Anggaran untuk Budget Implementasi ICT SKI:<br><br>1. Ditambahkan dalam penyertaan modal SI/ penambahan equity SI<br>2. Utang piutang (Jangka waktu)<br><br>bahwa tujuan diperlakukan sebagai tambahan penyertaan modal atau diperlakukan sebagai utang piutang agar tidak mengganggu anggaran operasional IT di SI, karena di tahun 2016 tidak ada anggaran untuk SKI dan untuk mengurangi beban/biaya di SI maupun SKI.<br><br>Pengambilan keputusan anggaran implementasi ICT-- Duedate W4 Oktober 2016 (PIC Pak Andik) ??<br></p>', 'Skenario Penggunaan Anggaran untuk Budget Implementasi ICT SKI:<br><br>1. Ditambahkan dalam penyertaan modal SI/ penambahan equity SI<br>2. Utang piutang (Jangka waktu)<br><br>bahwa tujuan diperlakukan sebagai tambahan penyertaan modal atau diperlakukan sebagai utang piutang agar tidak mengganggu anggaran operasional IT di SI, karena di tahun 2016 tidak ada anggaran untuk SKI dan untuk mengurangi beban/biaya di SI maupun SKI.<br><br>Pengambilan keputusan anggaran implementasi ICT-- Duedate W4 Oktober 2016 (PIC Pak Andik) ??<p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p><b></b></p><p></p>');

-- --------------------------------------------------------

--
-- Table structure for table `siuk_notulen_file`
--

CREATE TABLE IF NOT EXISTS `siuk_notulen_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_notulen` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `token` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `siuk_notulen_file`
--

INSERT INTO `siuk_notulen_file` (`id`, `id_notulen`, `name`, `token`) VALUES
(1, 2, 'Getting_Started.pdf', ''),
(2, 2, 'Tutorial_AngularJS_Lengkap_+_Script_Koding.pdf', '0.7217939846924233'),
(3, 2, 'Form_A_Kandidat.docx', '0.4994079736830064');

-- --------------------------------------------------------

--
-- Table structure for table `siuk_notulen_temp`
--

CREATE TABLE IF NOT EXISTS `siuk_notulen_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(50) NOT NULL,
  `token` varchar(500) NOT NULL,
  `sesi_form` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
