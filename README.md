# README #


### Setting Database ###

* Open **database.php** inside folder **app_siuk/application/config**
* Configure with your mysql database

### Change base_url ###

* Open **config.php** inside folder **app_siuk/application/config**
* Change base_url with your local project url
* Example : *http://localhost/siuk/*

### Don't forget to import mysql database ###